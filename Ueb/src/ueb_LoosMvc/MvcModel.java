package ueb_LoosMvc;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class MvcModel implements InterMvcModel {

	@Override
	public double berechneEndkapital(int startkapital, double zinssatz, double laufzeit) {
		double endkapital;
		endkapital = startkapital * Math.pow((1 + zinssatz / 100), laufzeit);
		return endkapital;
	}

	@Override
	public double berechneLaufzeit(int startkapital, double zinssatz, double endkapital) {
		double laufzeit;
		laufzeit = (Math.log(endkapital / startkapital)) / (Math.log(1 + zinssatz / 100));
		return laufzeit;
	}

	@Override
	public List<Double> berechnenJaerlicheZahlung(int startkapital, double zinssatz, double laufzeit) {
		List<Double> list = new ArrayList<>();
		double kapital = startkapital;

		for (int i = 1; i <= laufzeit; i++) {
			double zalungJahr = 0;
			zalungJahr = (kapital/100) * 5;
			kapital = kapital + zalungJahr;
			
			list.add(kapital);
			System.out.println(String.format("%,.2f", zalungJahr));
		}
		return list;
	}

}
