package ueb_LoosMvc;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

public class MvcView extends JFrame {
	private static final long serilVersionUID = 1L; /////////////
	// Controller als Member-Variable
	MvcController controller = new MvcController(this); //////////////////
	// Deklaration der Komponenten
	// Bezeichnungsfelder (Label)
	JLabel lblTitel;
	JLabel lblStartkapital;
	JLabel lblZinssatz;
	JLabel lblLaufzeit;
	JLabel lblEndkapital;

	JMenuBar menuBar;

	JMenu mnuDatei;
	JMenuItem mniBerechnen;
	JMenuItem mniLoeschen;
	JMenuItem mniBeenden;

	JMenu mnuInfo;
	JMenuItem mniInfo;

	JTextField txfStartkapital;
	JTextField txfZinssatz;
	JTextField txfLaufzeit;
	JTextField txfEndkapital;

	JButton btnBerechnen;
	JButton btnBeenden;
	JButton btnLoeschen;

	ButtonGroup btgModus;
	JRadioButton rbtEndkapital;
	JRadioButton rbtLaufzeit;

	JTextArea txaVerlauf;
	JScrollPane scpVerlauf;

	JPanel pnlInhalt;
	JPanel pnlButton;
	JPanel pnlTitel;
	JPanel pnlGesamt;
	JPanel pnlVerlauf;
	
	public MvcView() {
		super("MvcView");// Titelleiste
//		this.setTitle("Zinsrechner"); // Alternative
		initComponents();// Initialisierung der Componenten
//		Hinzuf�gen der Komponenten zu den Containern

		menuBar.add(mnuDatei);
		menuBar.add(mnuInfo);

		pnlTitel.add(lblTitel);

		pnlInhalt.add(lblStartkapital);
		pnlInhalt.add(txfStartkapital);

		pnlInhalt.add(lblZinssatz);
		pnlInhalt.add(txfZinssatz);

		pnlInhalt.add(lblLaufzeit);
		pnlInhalt.add(txfLaufzeit);

		pnlInhalt.add(lblEndkapital);
		pnlInhalt.add(txfEndkapital);

		pnlInhalt.add(rbtEndkapital);
		pnlInhalt.add(rbtLaufzeit);

		btgModus.add(rbtEndkapital);
		btgModus.add(rbtLaufzeit);

		pnlButton.add(btnBerechnen);
		pnlButton.add(btnLoeschen);
		pnlButton.add(btnBeenden);
		pnlVerlauf.add(scpVerlauf);
		
		pnlGesamt.add(pnlTitel);
		pnlGesamt.add(pnlInhalt);
		pnlGesamt.add(pnlButton);
		pnlGesamt.add(pnlVerlauf);
		
//		WindowListener f�r den JFrame
		this.addWindowListener(controller);
//		ActionListener f�r die Button und RadioButton mit Model-View-Controller
		btnBerechnen.addActionListener(controller);
		btnLoeschen.addActionListener(controller);
		btnBeenden.addActionListener(controller);
		rbtEndkapital.addActionListener(controller);
		rbtLaufzeit.addActionListener(controller);
		mniBerechnen.addActionListener(controller);
		mniLoeschen.addActionListener(controller);
		mniBeenden.addActionListener(controller);
		mniInfo.addActionListener(controller);

//		Layout des Panels setzen 
		pnlInhalt.setLayout(new GridLayout(5, 2, 30, 30));
		pnlGesamt.setLayout(new FlowLayout(FlowLayout.CENTER, 4000, 5));
		
		rbtEndkapital.setSelected(true);
		txfEndkapital.setEditable(false);
		txaVerlauf.setLineWrap(true);
		txaVerlauf.setWrapStyleWord(true);
		txaVerlauf.setFont(new Font("Courier New", Font.PLAIN, 12));
		scpVerlauf.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		pnlGesamt.setBackground(Color.ORANGE); // Hintergrundfarbe des Inhaltsbereichs
		pnlTitel.setOpaque(false); // Panel durchsichtig machen
		pnlInhalt.setOpaque(false);
		pnlButton.setOpaque(false);
		pnlVerlauf.setOpaque(false);
		rbtEndkapital.setOpaque(false);
		rbtLaufzeit.setOpaque(false);
		
		
//		Men� bauen
		mnuDatei.setMnemonic('D');
		mnuInfo.setMnemonic('i');
		mniBerechnen.setMnemonic(KeyEvent.VK_B); 
		mniLoeschen.setMnemonic(KeyEvent.VK_L); 
		mniBeenden.setMnemonic(KeyEvent.VK_E); ///// ?????
		mniInfo.setMnemonic(KeyEvent.VK_I); ///// ?????
		mniInfo.setAccelerator(KeyStroke.getKeyStroke("control I"));
		mniBerechnen.setAccelerator(KeyStroke.getKeyStroke("control B"));
		mniLoeschen.setAccelerator(KeyStroke.getKeyStroke("control L"));
		mniBeenden.setAccelerator(KeyStroke.getKeyStroke("control E"));
		
		mnuDatei.add(mniBerechnen);
		mnuDatei.add(mniLoeschen);
		mnuDatei.add(mniBeenden);
		mnuInfo.add(mniInfo);
		menuBar.add(mnuDatei);
		menuBar.add(mnuInfo);
		this.setJMenuBar(menuBar);
		
//		Setzen des Panels als Inhaltsbereich 
		this.setContentPane(pnlGesamt);
		this.setSize(350, 500);// Gr��e des Fensters in Breite mal H�he
		this.setLocationRelativeTo(null);// Fenster wird mittig angeordnet
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);// Schlie�en des Fensters wird durch Listener abgedeckt
		this.setVisible(true);// Fesnster wird sichtbar gemacht
	}

	private void initComponents() {
		lblTitel = new JLabel("Mein Fenster");

		lblStartkapital = new JLabel("Startkapital");
		lblZinssatz = new JLabel("Zinssatz");
		lblLaufzeit = new JLabel("Laufzeit");
		lblEndkapital = new JLabel("Endkapital");

		txfStartkapital = new JTextField(10);
		txfZinssatz = new JTextField(10);
		txfLaufzeit = new JTextField(10);
		txfEndkapital = new JTextField(10);

		btnBerechnen = new JButton("Berechnen");
		btnLoeschen = new JButton("Loeschen");
		btnBeenden = new JButton("Beenden");

		rbtEndkapital = new JRadioButton("Endkapital");
		rbtLaufzeit = new JRadioButton("Laufzeit");
		btgModus = new ButtonGroup();

		txaVerlauf = new JTextArea(6, 30);
		scpVerlauf = new JScrollPane(txaVerlauf);

		pnlInhalt = new JPanel();
		pnlTitel = new JPanel();
		pnlButton = new JPanel();
		pnlGesamt = new JPanel();
		pnlVerlauf = new JPanel();


		menuBar = new JMenuBar();
		mnuDatei = new JMenu("Datei");
		mnuInfo = new JMenu("Info");
		mniBerechnen = new JMenuItem("Berechnen");
		mniLoeschen = new JMenuItem("Loeschen");
		mniBeenden = new JMenuItem("Beenden");
		mniInfo = new JMenuItem("Info", KeyEvent.VK_I);
		
		controller.init();
	}
}
