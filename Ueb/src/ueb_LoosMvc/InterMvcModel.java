package ueb_LoosMvc;

import java.util.List;

public interface InterMvcModel {

	public double berechneEndkapital (int startkapital, double zinssatz, double laufzeit);

	public double berechneLaufzeit (int startkapital, double zinssatz, double endkapital);
	
	public List<Double> berechnenJaerlicheZahlung(int startkapital, double zinssatz, double laufzeit); 
	
}
