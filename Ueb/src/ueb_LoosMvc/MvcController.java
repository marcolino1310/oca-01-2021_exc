package ueb_LoosMvc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.NumberFormat;
import java.util.List;

import javax.swing.JOptionPane;

public class MvcController extends WindowAdapter implements ActionListener, KeyListener {

	MvcModel model; /////////////
	MvcView view; ////////////

	public MvcController(MvcView view) { ///////////////
		this.view = view; //////////////
		this.model = new MvcModel(); /////////////

	}

	public void init() {
		inArea("Hallo!");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("Beenden")) {
			beenden();
		} else {
			if (e.getSource() == view.btnBerechnen || e.getSource() == view.mniBerechnen) {
				berechnen();
			} else {
				if (e.getSource() == view.rbtEndkapital) {
					view.txfEndkapital.setText("");
					view.txfEndkapital.setEditable(false);
					view.txfLaufzeit.setEditable(true);

				} else {
					if (e.getSource() == view.rbtLaufzeit) {
						view.txfLaufzeit.setText("");
						view.txfLaufzeit.setEditable(false);
						view.txfEndkapital.setEditable(true);
					} else {
						if (e.getActionCommand().equals("Loeschen")) {
						//(e.getSource() == view.btnLoeschen || e.getSource() == view.mniLoeschen) {
							loeschen();
						} else {
							if (e.getActionCommand().equals("Info")) {
								info();
							}
						}
					}
				}
			}
		}
	}

	private void Meesge(String messag) {
		JOptionPane.showMessageDialog(view, messag , "Info",
				JOptionPane.YES_NO_OPTION);
	}
	
	private void info() {
		JOptionPane.showMessageDialog(view, "� 2021 Comcave" + System.lineSeparator() + "Juri Loos", "Info",
				JOptionPane.YES_NO_OPTION);
	}

	private void berechnen() {
		int startkapital;
		double zinssatz;
		double laufzeit =0; 
		double endkapital;
		String str;
		try {
			startkapital = Integer.parseInt(view.txfStartkapital.getText());
			zinssatz = Double.parseDouble(view.txfZinssatz.getText());
		} catch (NumberFormatException e) {
			Meesge("Error");
			return;
		}
		if (view.rbtEndkapital.isSelected()) {
			str = view.txfLaufzeit.getText().replace(".", "");
			str = str.replace(",", ".");
			laufzeit = Double.parseDouble(str);
			try {
			endkapital = model.berechneEndkapital(startkapital, zinssatz, laufzeit);
			} catch (NumberFormatException e) {
				Meesge("Error");
				return;
			}
			List<Double> list = model.berechnenJaerlicheZahlung(startkapital, zinssatz, laufzeit);
			view.txaVerlauf.setText(null);
//			int jahr = 1;
//			for (Double zul : list) {
//				inArea(jahr + "       " + String.format("%,.2f", zul)); // zul);
//				jahr += 1;
//			}

			str = String.format("%,.2f", endkapital);
			// str = NumberFormat.getCurrencyInstance().format(endkapital);
			view.txfEndkapital.setText(str);
		}

		if (view.rbtLaufzeit.isSelected()) {
			str = view.txfEndkapital.getText().replace(".", "");
			str = str.replace(",", ".");
			try {
				endkapital = Double.parseDouble(str);
			} catch (NumberFormatException e) {
				view.txaVerlauf.setText(null);
				inArea("Information ist falsch");
				return;
			}
			laufzeit = model.berechneLaufzeit(startkapital, zinssatz, endkapital);

			str = String.format("%,.2f", laufzeit);
			// str = NumberFormat.getCurrencyInstance().format(laufzeit);
			view.txfLaufzeit.setText(str);
		}
		
		StringBuilder sbVerlauf = new StringBuilder();
		for (int jahr = 1; jahr <= laufzeit; jahr++) {
			sbVerlauf.append(String.format("%2d\t%,10.2f �%n", jahr, model.berechneEndkapital(startkapital, zinssatz, jahr)));
		}
		view.txaVerlauf.setText(sbVerlauf.toString());
		
	}

	private void beenden() {
		int antwort = JOptionPane.showConfirmDialog(view, "Sind Sie sicher?", "Beenden", JOptionPane.YES_NO_OPTION);
		if (antwort == JOptionPane.YES_OPTION)
			System.exit(0);
	}

	private void loeschen() {
		view.txfLaufzeit.setText("");
		view.txfStartkapital.setText("");
		view.txfZinssatz.setText("");
		view.txfEndkapital.setText("");
		view.txfStartkapital.requestFocus();
		view.txaVerlauf.setText(null);
		view.rbtEndkapital.setSelected(true);
		view.txfEndkapital.setEditable(false);
		view.txfLaufzeit.setEditable(true);
	}

	private void inArea(String str) {
		view.txaVerlauf.append(str);
		view.txaVerlauf.append("\n");
	}

	@Override
	public void windowClosing(WindowEvent e) {
		beenden();
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getKeyChar() == KeyEvent.VK_ENTER) {
			if (e.getSource() == view.txfStartkapital) {
				view.txfZinssatz.requestFocus();
			} else if (e.getSource() == view.txfZinssatz) {
				if (view.rbtEndkapital.isSelected()) {
					view.txfLaufzeit.requestFocus();	
				} else if (view.rbtLaufzeit.isSelected()) {
					view.txfEndkapital.requestFocus();
				}
			} else if (e.getSource() == view.txfLaufzeit || e.getSource() == view.txfEndkapital) {
				berechnen();
			}
		}	
		
	}

}
