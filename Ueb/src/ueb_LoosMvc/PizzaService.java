package ueb_LoosMvc;

import java.text.ParseException;
import java.util.ArrayList;

public class PizzaService {

	public static void main(String[] belaege) {
		String[] belaege2;
		 belaege2 = belaege;
		 
//		belaege2 = new String[3]; // !!!!!!!!!!!!!!!!!!!
//		belaege2[0] = "Tomaten";
//		belaege2[1] = "Pilze";
//		belaege2[2] = "Schinken";

		if (belaege2.length == 0) {
			System.out.println("belaege eingegeben ");
			return;
		}

		if (belaege2.length > 3) {
			System.out.println("Sehr viel belaege");
			return;
		}

		try {
			for (String str : belaege2) {
				// System.out.println(str);
				if (contains(str)) {
					// System.out.println(str);
				} else {
					System.out.println("Keine Belag : " + str);
					throw new InvalidBelagException();
				}
			}

		} catch (InvalidBelagException ex) {
			return;
		}
		
		
		System.out.println("Sie habe Bestellung gemacht.");
		System.out.println("Mit Belagen: ");

		for (String str : belaege2) {
			System.out.println(str);
		}
	}

	public static boolean contains(String test) {
		for (Belag c : Belag.values()) {
			if (c.name().equals(test)) {
				return true;
			}
		}
		return false;
	}
}

enum Belag {
	Tomaten, Pilze, Salami, Schinken, Artischocken, Kaese, Broccoli
}

class InvalidBelagException extends Exception {
	InvalidBelagException() {
	}
}
