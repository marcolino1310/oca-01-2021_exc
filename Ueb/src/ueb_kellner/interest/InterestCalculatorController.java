package ueb_kellner.interest;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import com.sun.corba.se.impl.ior.GenericTaggedComponent;

public class InterestCalculatorController extends WindowAdapter implements ActionListener, KeyListener {

	private Locale locale = Locale.GERMANY;

	InterestCalculatorView view;
	InterestCalculatorModel model;

	public InterestCalculatorController(InterestCalculatorView view) {
		this.view = view;
		this.model = new InterestCalculatorModel();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == view.btnExit || e.getSource() == view.miExit) {
			exit();
		}
		if (e.getSource() == view.btnCalculate || e.getSource() == view.miCalculate) {
			calculate(e);
		}
		if (e.getSource() == view.rbtnDuration) {
			flipActiveRbtn(view.txfFinalCapital, view.txfDuration);
		}
		if (e.getSource() == view.rbtnFinalCapital) {
			flipActiveRbtn(view.txfDuration, view.txfFinalCapital);
		}
		if (e.getSource() == view.btnReset || e.getSource() == view.miReset) {
			reset();
		}
		if (e.getSource() == view.miInfo) {
			infoMessage();
		}

	}

	private void flipActiveRbtn(JTextField activate, JTextField inactivate) {
		inactivate.setEditable(false);
		activate.setEditable(true);

//		activate.setFocusTraversalKeysEnabled(true);

		activate.requestFocusInWindow();
		activate.selectAll();
	}

	private void reset() {
		view.txfCapital.setText("");
		model.setCapital(0);
		view.txfInterestRate.setText("");
		model.setInterestRate(0);
		view.txfDuration.setText("");
		model.setDuration(0);
		view.txfFinalCapital.setText("");
		model.setFinalCapital(0);

		view.txaResult.setText("");
		view.txfCapital.requestFocusInWindow();
	}

	private void exit() {
//		int result = JOptionPane.showConfirmDialog(view, "Wollen Sie wirklich beenden", "Beenden",
//				JOptionPane.YES_NO_OPTION);
//		if (result == JOptionPane.YES_OPTION)
		System.exit(0);
	}

	private void calculate(ActionEvent e) {
		try {
			model.setCapital(NumberFormat.getNumberInstance(locale).parse(view.txfCapital.getText()).doubleValue());
			model.setInterestRate(
					NumberFormat.getNumberInstance(locale).parse(view.txfInterestRate.getText()).doubleValue());
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if (view.rbtnDuration.isSelected()) {
			calcDuration();
		}

		if (view.rbtnFinalCapital.isSelected()) {
			calcFinalCapital();
		}
	}

	private void calcFinalCapital() {

		try {
			model.setDuration(NumberFormat.getNumberInstance(locale).parse(view.txfDuration.getText()).doubleValue());
		} catch (ParseException pe) {
			// TODO Auto-generated catch block
			pe.printStackTrace();
		}
		try {
			view.txfFinalCapital.setText(String.format(locale, "%,.2f", model.calculateFinalCapital()));
		} catch (ValueMissingException vme) {
			// TODO Auto-generated catch block
			vme.printStackTrace();
		}
		setOutput();
	}

	private void calcDuration() {
		try {
			model.setFinalCapital(
					NumberFormat.getNumberInstance(locale).parse(view.txfFinalCapital.getText()).doubleValue());
		} catch (ParseException pe) {
			// TODO Auto-generated catch block
			pe.printStackTrace();
		}

		try {
			view.txfDuration.setText(String.format(locale, "%,.1f", model.calculateDuration()));
		} catch (ValueMissingException vme) {
			// TODO Auto-generated catch block
			vme.printStackTrace();
		}
		setOutput();
	}

	private void setOutput() {
//		view.txaResult.setText("");//if ta.append
		StringBuilder sb = new StringBuilder();
		int max = 0;
		try {
			max = NumberFormat.getNumberInstance(locale).parse(view.txfDuration.getText()).intValue();
		} catch (ParseException pe) {
			// TODO Auto-generated catch block
			pe.printStackTrace();
		}
		for (int i = 1; i < max + 1; ++i) {
			model.setDuration(i);

			try {//TODO reject values > 999.999.999
//				view.txaResult.append(String.format(locale, "%3d\t", i));
//				view.txaResult.append(
//						String.format(locale, "%, 18.2f", model.calculateFinalCapital()) + System.lineSeparator());
				sb.append(String.format(locale, "%1$3d\t%2$, 18.2f", i, model.calculateFinalCapital())
						+ System.lineSeparator());
			} catch (ValueMissingException vme) {
				// TODO Auto-generated catch block
				vme.printStackTrace();
			}
		}
		view.txaResult.setText(sb.toString().substring(0, sb.length() - 1));
	}

	@Override
	public void windowClosing(WindowEvent e) {
		exit();
	}

	public void infoMessage() {
		JOptionPane.showMessageDialog(view, "Zinsrechner" + System.lineSeparator() + "Versoin 1.0", "Info",
				JOptionPane.INFORMATION_MESSAGE);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

		if (e.getKeyCode() == KeyEvent.VK_ALT)
			System.out.println("enter " + e.getKeyCode() + " | " + KeyEvent.VK_ENTER);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub

//		System.out.println("enter " + e.getKeyChar() + " | " + KeyEvent.VK_ENTER);
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		System.out.println("released: " + e.getKeyCode());
	}

}
