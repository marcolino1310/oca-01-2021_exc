package ueb_kellner.interest;

public interface I_InterestCalculatorModel {

	double calculateFinalCapital() throws ValueMissingException;

	double calculateDuration() throws ValueMissingException;
}
