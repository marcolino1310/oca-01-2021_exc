package ueb_kellner.interest;

public class InterestCalculatorModel implements I_InterestCalculatorModel {

	private double capital = 0;
	private double interestRate = 0;
	private double duration = 0;
	private double finalCapital = 0;

	@Override
	public double calculateFinalCapital() throws ValueMissingException {
		if (!(capital > 0) || !(interestRate > 0) || !(duration > 0))
			throw new ValueMissingException("Cannot calculate with: " + capital + " " + interestRate + " " + duration);
		return capital * (Math.pow(1.0 + (interestRate / 100.0), duration));

	}

	@Override
	public double calculateDuration() throws ValueMissingException {
		if (!(capital > 0) || !(interestRate > 0) || !(finalCapital > 0))
			throw new ValueMissingException(
					"Cannot calculate with: " + capital + " " + interestRate + " " + finalCapital);

		return Math.log(finalCapital / capital) / Math.log(1 + interestRate / 100);
	}

	public double getCapital() {
		return capital;
	}

	public void setCapital(double capital) {
		this.capital = capital;
	}

	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	public double getDuration() {
		return duration;
	}

	public void setDuration(double duration) {
		this.duration = duration;
	}

	public double getFinalCapital() {
		return finalCapital;
	}

	public void setFinalCapital(double finalCapital) {
		this.finalCapital = finalCapital;
	}
}