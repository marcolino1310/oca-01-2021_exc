package ueb_kellner.interest;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.KeyEvent;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class InterestCalculatorView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Font font = new Font("Courier New", Font.PLAIN, 12);

	JLabel lblHead;
	JLabel lblCapital;
	JLabel lblInterestRate;
	JLabel lblDuration;
	JLabel lblFinalCapital;

	JTextField txfCapital;
	JTextField txfInterestRate;
	JTextField txfDuration;
	JTextField txfFinalCapital;

	ButtonGroup rbgDurationFinalCapital;
	JRadioButton rbtnDuration;
	JRadioButton rbtnFinalCapital;

	JButton btnCalculate;
	JButton btnReset;
	JButton btnExit;

	JTextArea txaResult;
	JScrollPane spResult;

	JMenuBar mb;
	JMenu mFile;
	JMenuItem miCalculate;
	JMenuItem miReset;
	JMenuItem miExit;
	JMenu mInfo;
	JMenuItem miInfo;

	JPanel pnlHead;
	JPanel pnlBody;
	JPanel pnlButtons;
	JPanel pnlOutput;
	JPanel pnlContent;

	InterestCalculatorController controller = new InterestCalculatorController(this);

	public InterestCalculatorView() throws HeadlessException {
		super("Zinsrechner");

		initComponents();
		this.setJMenuBar(mb);
		mb.add(mFile);
		mFile.add(miCalculate);
		mFile.add(miReset);
		mFile.add(miExit);
		mb.add(mInfo);
		mInfo.add(miInfo);

		pnlHead.add(lblHead);

		pnlBody.add(lblCapital);
		pnlBody.add(txfCapital);

		pnlBody.add(lblInterestRate);
		pnlBody.add(txfInterestRate);

		pnlBody.add(lblDuration);
		pnlBody.add(txfDuration);

		pnlBody.add(lblFinalCapital);
		pnlBody.add(txfFinalCapital);

		pnlBody.add(rbtnFinalCapital);
		pnlBody.add(rbtnDuration);

		rbgDurationFinalCapital.add(rbtnFinalCapital);
		rbgDurationFinalCapital.add(rbtnDuration);

		pnlButtons.add(btnCalculate);
		pnlButtons.add(btnReset);
		pnlButtons.add(btnExit);

		pnlOutput.add(spResult);

		pnlContent.add(pnlHead);
		pnlContent.add(pnlBody);
		pnlContent.add(pnlButtons);
		pnlContent.add(pnlOutput);

		txfCapital.addKeyListener(controller);
		txfInterestRate.addKeyListener(controller);
		txfDuration.addKeyListener(controller);
		txfFinalCapital.addKeyListener(controller);

		rbtnDuration.addActionListener(controller);
		rbtnFinalCapital.addActionListener(controller);

		btnCalculate.addActionListener(controller);
		btnExit.addActionListener(controller);
		btnReset.addActionListener(controller);

		miCalculate.addActionListener(controller);
		miReset.addActionListener(controller);
		miExit.addActionListener(controller);
		miInfo.addActionListener(controller);
		
		this.addWindowListener(controller);

		
		rbtnFinalCapital.doClick();

		pnlHead.setOpaque(false);
		rbtnDuration.setOpaque(false);
		rbtnFinalCapital.setOpaque(false);
		spResult.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		pnlBody.setLayout(new GridLayout(5, 2, 25, 10));
		pnlBody.setOpaque(false);
		pnlButtons.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 0));
		pnlButtons.setOpaque(false);
		pnlOutput.setOpaque(false);
		pnlContent.setLayout(new FlowLayout(FlowLayout.CENTER, 2_000, 30));
		pnlContent.setBackground(Color.orange);

		this.setContentPane(pnlContent);
		this.setSize(350, 550);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
//		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setVisible(true);
	}

	private void initComponents() {
		mb = new JMenuBar();

		mFile = new JMenu("Datei");
		mFile.setMnemonic(KeyEvent.VK_ALT);
		miCalculate = new JMenuItem("Berechnen");
		miCalculate.setMnemonic(KeyEvent.VK_B);
		miReset = new JMenuItem("Zurücksetzen");
		miReset.setMnemonic(KeyEvent.VK_Z);
		miExit = new JMenuItem("Beenden");
		miExit.setMnemonic(KeyEvent.VK_E);

		mInfo = new JMenu("Info");
		mInfo.setMnemonic(KeyEvent.VK_I);
		miInfo = new JMenuItem("Info");
		miInfo.setMnemonic(KeyEvent.VK_I);

		lblHead = new JLabel("Zinsrechner");
		lblHead.setFont(font);

		lblCapital = new JLabel("Startkapital");
		lblCapital.setFont(font);
		lblInterestRate = new JLabel("Zinssatz");
		lblInterestRate.setFont(font);
		lblDuration = new JLabel("Verzinsungsdauer");
		lblDuration.setFont(font);
		lblFinalCapital = new JLabel("Endkapital");
		lblFinalCapital.setFont(font);

		txfCapital = new JTextField(18);
		txfCapital.setHorizontalAlignment(JTextField.RIGHT);
		txfCapital.setFont(font);
		txfInterestRate = new JTextField(18);
		txfInterestRate.setHorizontalAlignment(JTextField.RIGHT);
		txfInterestRate.setFont(font);
		txfDuration = new JTextField(18);
		txfDuration.setHorizontalAlignment(JTextField.RIGHT);
		txfDuration.setFont(font);
		txfFinalCapital = new JTextField(18);
		txfFinalCapital.setHorizontalAlignment(JTextField.RIGHT);
		txfFinalCapital.setFont(font);

		// TODO remove opinionated presets
		txfCapital.setText("1000");
		txfCapital.selectAll();
		txfInterestRate.setText("5");
		txfDuration.setText("50");

		rbgDurationFinalCapital = new ButtonGroup();
		rbtnFinalCapital = new JRadioButton("Endkapital");
		rbtnFinalCapital.setFont(font);
		rbtnDuration = new JRadioButton("Laufzeit");
		rbtnDuration.setFont(font);

		btnCalculate = new JButton("Berechnen");
		btnCalculate.setFont(font);
		btnReset = new JButton("Zurücksetzen");
		btnReset.setFont(font);
		btnExit = new JButton("Beenden");
		btnExit.setFont(font);

		txaResult = new JTextArea(10, 30);
		txaResult.setFont(font);
		txaResult.setEditable(false);
		spResult = new JScrollPane(txaResult);

		pnlHead = new JPanel();
		pnlBody = new JPanel();
		pnlButtons = new JPanel();
		pnlOutput = new JPanel();
		pnlContent = new JPanel();

	}
}