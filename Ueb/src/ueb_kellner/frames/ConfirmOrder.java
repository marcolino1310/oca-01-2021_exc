package ueb_kellner.frames;

import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class ConfirmOrder extends JFrame{

	private static final long serialVersionUID = 1L;
	private JButton btnConfirm = new JButton("Confirm");
	private JLabel lblTitle = new JLabel("Do you really want to order such pizza?");
	
	public ConfirmOrder(String title) {
		super(title); // Titelleiste
		this.setSize(275, 100); // Breite mal H�he
		this.setDefaultCloseOperation(EXIT_ON_CLOSE); // Was passiert beim Schlie�en des Frames?
		this.setLocationRelativeTo(null); // Positon des Frames mittig im Bildschirm
		this.getContentPane().setBackground(Color.BLUE);// Hintergrundfarbe der "Inhaltsscheibe" setzen
		this.getContentPane().setLayout(new FlowLayout());
		this.getContentPane().add(lblTitle);
		this.getContentPane().add(getBtnConfirm());
		this.getBtnConfirm().addActionListener((e)->{getBtnConfirm();});
		this.setResizable(false);
		
		
		this.setVisible(true); // sichtbar (default: nicht sichtbar)
	}

	public JButton getBtnConfirm() {
		return btnConfirm;
	}
	
}
