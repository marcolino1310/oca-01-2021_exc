package ueb_kellner;

public class SomeClass implements I_Inter{
//	public static void main(String[] args) {
//		
//	}
	{
		System.out.println("this is a class method");
	}
}
interface I_Inter {
	public static void main(String... args) {
		new SomeClass();
		System.out.println("this is from within the interface");
	};
	
}