package ueb_kellner.exceptions;

public class InvalidToppingException extends PizzaException {
	public InvalidToppingException(String message) {
		super(message);
	}
}