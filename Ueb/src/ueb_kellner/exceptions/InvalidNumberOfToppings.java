package ueb_kellner.exceptions;

public class InvalidNumberOfToppings extends PizzaException {
	public InvalidNumberOfToppings(String message) {
		super(message);
	}
}