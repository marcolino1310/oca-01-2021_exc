package ueb_kellner.pizza;

import ueb_kellner.exceptions.InvalidNumberOfToppings;
import ueb_kellner.exceptions.InvalidToppingException;
import ueb_kellner.exceptions.PizzaException;

public class RunPizza {
	public static void main(String[] args) throws PizzaException {
		// checking
		if (args.length < 1 || args.length > 3) {
			throw new InvalidNumberOfToppings("Must be 1, 2 or 3: " + args.length);
		}
		for (String str : args) {
			if (!PizzaToppings.contains(str))
				throw new InvalidToppingException("We don't carry that topping: " + str);
		}
		// formatting
		for (int i = 0; i < args.length; i++) {
			args[i] = args[i].substring(0, 1).toUpperCase() + args[i].substring(1).toLowerCase();
		}

		System.out.println("Proccesing Your pizza: \n");
		for (int i = 0; i < args.length; i++) {
			System.out.print("Belege");
			for (int j = 0; j < 10; j++) {
				try {
					Thread.sleep(250);
					System.out.print(".");
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println();
			System.out.println(args[i]);
		}

		System.out.println("Your Pizza is beeing baked");
		for (int i = 0; i < 10; i++) {
			try {
				System.out.print("#");
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println();
		System.out.println();
		System.out.print("Pizza ");
		for (int i = 0; i < args.length; i++) {
			System.out.print(args[i]+ " ");
		}
		System.out.print("ready");
	}
}