package ueb_kellner.pizza;

public enum PizzaToppings {
	TOMATEN, PILZE, SALAMI, SCHINKEN, ARTISCHOCKEN, K�SE, BROCCOLI;

	public static boolean contains(String test) {
		for (PizzaToppings topping : PizzaToppings.values()) {
			if (topping.toString().equals(test.toUpperCase()))
				return true;
		}
		return false;
	}
}
