package ueb_kellner.zipCodeMaster;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ZipDbModel extends ZipModel {

	private String accessConnect = "jdbc:ucanaccess://files\\PLZ_Datenbank.accdb";
	private Connection connection;
	private Statement statement;

	ZipDbModel() {
		try {
			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
			connection = DriverManager.getConnection(accessConnect);
			connection.setReadOnly(true);
			System.out.println("Connection to: " + connection.getMetaData() + " established");
			statement = connection.createStatement();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	ArrayList<String[]> congestResult(ResultSet rs) throws SQLException {
		ArrayList<String[]> result = new ArrayList<>();
		while (rs.next()) {
			String[] location = new String[rs.getMetaData().getColumnCount()];
			for (int i = 0; i < rs.getMetaData().getColumnCount(); ++i) {
				location[i] = rs.getString(i + 1);
			}
			result.add(location);
		}
		return result;
	}

	@Override
	ArrayList<String[]> advancedSearch(String[] searchTerm) {
		searchTerm[2] = searchTerm[2] == "Alle" ? "%" : searchTerm[2];
		try {
			ResultSet rs = statement.executeQuery("SELECT `PLZ`,`Ort`,`Bundesland` FROM `tblPLZ` "
					+ "WHERE `PLZ` LIKE \"" + searchTerm[0] + "%\"" + "AND `Ort` LIKE \"%" + searchTerm[1] + "%\""
					+ "AND `Bundesland` LIKE \"%" + searchTerm[2] + "%\";");
			return congestResult(rs);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	@Override
	ArrayList<String[]> fuzzySearch(String searchTerm) {
		try {
			ResultSet rs = statement
					.executeQuery("SELECT DISTINCT `PLZ`,`Ort`,`Bundesland` FROM `tblPLZ` WHERE `PLZ` LIKE \""
							+ searchTerm + "%\"" + "OR `Ort` LIKE \"%" + searchTerm + "%\"" + "OR `Bundesland` LIKE \"%"
							+ searchTerm + "%\";");
			return congestResult(rs);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	@Override
	ArrayList<String> getAllStates() {
		try {
			ResultSet rs = statement.executeQuery("SELECT DISTINCT `Bundesland` FROM `tblPLZ`;");
			ArrayList<String> result = new ArrayList<>();
			for (String[] str : congestResult(rs)) {
				result.add(str[0]);
			}
			return result;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}
}
