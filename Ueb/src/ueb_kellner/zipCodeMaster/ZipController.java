package ueb_kellner.zipCodeMaster;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JOptionPane;

import ueb_kellner.zipCodeMaster.ZipModel.Mode;

public class ZipController implements KeyListener, ActionListener, FocusListener, MouseListener {

	ZipView view;
	ZipModel model;

	public ZipController(ZipView view, Mode mode) {
		this.view = view;
		setDataStoreMode(mode);
	}

	private void advancedSearch() {
		view.tmodResult.setRowCount(0);
		for (String[] str : model.advancedSearch(new String[] { view.txfSearchZip.getText(),
				view.txfSearchCity.getText(), view.cboxState.getSelectedItem().toString() })) {
			view.tmodResult.insertRow(view.tmodResult.getRowCount(), str);
		}
	}

	private void fuzzySearch() {
		view.tmodResult.setRowCount(0);
		for (String[] str : model.fuzzySearch(view.txfSearchFuzzy.getText())) {
			view.tmodResult.addRow(str);
		}
	}

	private void switchSearchMode() {
		if (view.pnlFuzzyBody.isVisible()) {
			view.pnlFuzzyBody.setVisible(false);
			view.pnlAdvancedBody.setVisible(true);
			view.miSearchModeFuzzy.setVisible(false);
			view.miSearchModeAdvanced.setVisible(true);
			view.txfSearchZip.requestFocusInWindow();
		} else {
			view.pnlAdvancedBody.setVisible(false);
			view.pnlFuzzyBody.setVisible(true);
			view.miSearchModeAdvanced.setVisible(false);
			view.miSearchModeFuzzy.setVisible(true);
			view.txfSearchFuzzy.requestFocusInWindow();
		}
		view.validate();
	}

	private void setDataStoreMode(Mode mode) {
		this.model = ZipModel.getInstance(mode);
	}

	private void switchDataStoreMode() {
		if (this.model instanceof ZipFileModel) {
			setDataStoreMode(Mode.DB);
			view.miDataStoreModeFile.setVisible(false);
			view.miDataStoreModeDB.setVisible(true);
			view.setBackground(view.clrDataModeDB);
		} else {
			setDataStoreMode(Mode.FILE);
			view.miDataStoreModeDB.setVisible(false);
			view.miDataStoreModeFile.setVisible(true);
			view.setBackground(view.clrDataModeFile);
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getSource() == view.txfSearchFuzzy)
			fuzzySearch();
		else
			advancedSearch();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == view.cboxState) {
			advancedSearch();
		}
		if (e.getActionCommand().equals(view.miDataStoreModeDB.getText())
				|| e.getActionCommand().equals(view.miDataStoreModeFile.getText())) {
			switchDataStoreMode();
		}
		if (e.getActionCommand().equals(view.miSearchModeFuzzy.getText())
				|| e.getActionCommand().equals(view.miSearchModeAdvanced.getText())) {
			switchSearchMode();
		}
		if (e.getActionCommand().equals(view.miHelp.getText())) {
			JOptionPane.showMessageDialog(view, view.txaHelp, "Hilfe", JOptionPane.INFORMATION_MESSAGE);
		}
	}

	@Override
	public void focusGained(FocusEvent e) {
		if (e.getSource() == view.txfSearchFuzzy) {
			fuzzySearch();
		} else {
			advancedSearch();
		}
	}

	@Override
	public void focusLost(FocusEvent e) {
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (e.getSource() == view.pnlFuzzyBody || e.getSource() == view.pnlAdvancedBody) {
			switchSearchMode();
		}

	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

}
