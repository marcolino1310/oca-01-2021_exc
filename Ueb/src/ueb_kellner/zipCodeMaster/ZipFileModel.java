package ueb_kellner.zipCodeMaster;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class ZipFileModel extends ZipModel {

	private final String dataFile = "files\\plz.txt";
	private ArrayList<ZipLocation> locations = new ArrayList<>();

	ZipFileModel() {
		locations = readZipCityState();
		System.out.println(dataFile + " successfully imported");
	}

	private ArrayList<ZipLocation> readZipCityState() {
		ArrayList<ZipLocation> result = new ArrayList<ZipLocation>();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(dataFile),
					"UTF8")); /*
								 * Dekoration des FilesReader mit einem BufferedReader => mehr komfortable
								 * Methoden, bessere Performance
								 */
			String line;
			while ((line = br.readLine()) != null) {
				String[] lineElements = line.split("\t");
				result.add(ZipLocation.of(lineElements[0], lineElements[1], lineElements[5]));
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}

	private ArrayList<ZipLocation> findZip(String zip) {
		ArrayList<ZipLocation> result = new ArrayList<ZipLocation>();
		for (ZipLocation location : locations) {
			if (location.zip.substring(0, zip.length()).equals(zip)) {
				result.add(location);
			}
		}

		return result;
	}

	private ArrayList<ZipLocation> findCity(String searchTerm) {
		ArrayList<ZipLocation> result = new ArrayList<ZipLocation>();
		for (ZipLocation location : locations) {
			if (location.city.length() >= searchTerm.length()
					&& location.city.toLowerCase().contains(searchTerm.toLowerCase())) {
				result.add(location);
			}
		}
		return result;
	}

	private ArrayList<ZipLocation> findState(String searchTerm) {
		ArrayList<ZipLocation> result = new ArrayList<>();
		for (ZipLocation location : locations) {
			if (location.state.length() >= searchTerm.length()
					&& location.state.toLowerCase().contains(searchTerm.toLowerCase())) {
				result.add(location);
			}
		}
		return result;
	}

	private ArrayList<ZipLocation> intersectionOf(ArrayList<ZipLocation> locations1,
			ArrayList<ZipLocation> locations2) {
		ArrayList<ZipLocation> result = new ArrayList<ZipLocation>();

		if (locations1.size() == 0 || locations2.size() == 0)
			return result;

		for (ZipLocation location : locations1) {
			if (locations2.contains(location)) {
				result.add(location);
			}
		}
		return result;
	}

	private ArrayList<String[]> convert(ArrayList<ZipLocation> source) {
		ArrayList<String[]> result = new ArrayList<String[]>();
		source.forEach(location -> result.add(location.toStringArray()));
		return result;
	}

	@Override
	ArrayList<String[]> advancedSearch(String[] searchTerms) {

		if (searchTerms[0].length() == 0 && searchTerms[1].length() == 0 && searchTerms[2].equals("Alle")) {
			return convert(locations);
		}

		ArrayList<ZipLocation> matches = new ArrayList<>();

		if (searchTerms[0].length() > 0) {
			matches = findZip(searchTerms[0]);
			if (matches.size() == 0)
				return convert(matches);
		}

		if (searchTerms[1].length() > 0) {
			if (matches.size() == 0) {
				matches = findCity(searchTerms[1]);
			} else {
				matches = intersectionOf(findCity(searchTerms[1]), matches);
				if (matches.size() == 0)
					return convert(matches);
			}
		}

		if (!searchTerms[2].equals("Alle")) {
			if (matches.size() == 0) {
				matches = findState(searchTerms[2]);
			} else {
				matches = intersectionOf(findState(searchTerms[2]), matches);
			}
		}
		return convert(matches);
	}

	@Override
	ArrayList<String[]> fuzzySearch(String searchTerm) {

		if (searchTerm.length() == 0) {
			return convert(locations);
		}

		ArrayList<ZipLocation> matches = null;

		try {
			Integer.parseInt(searchTerm);
			matches = findZip(searchTerm);

		} catch (Exception e) {
			matches = findCity(searchTerm);

			for (ZipLocation location : findState(searchTerm)) {
				if (!matches.contains(location))
					matches.add(location);
			}
		}

		matches.sort(null);

		return convert(matches);
	}

	@Override
	ArrayList<String> getAllStates() {
		ArrayList<String> result = new ArrayList<>();
		for (ZipLocation loc : locations) {
			if (!result.contains(loc.state)) {
				result.add(0, loc.state);
			}
		}
		result.sort(null);
		return result;
	}

}
