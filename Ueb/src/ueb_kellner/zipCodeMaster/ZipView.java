package ueb_kellner.zipCodeMaster;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import ueb_kellner.zipCodeMaster.ZipModel.Mode;

public class ZipView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Font font = new Font("Courier New", Font.PLAIN, 12);
	Color clrDataModeDB = Color.getHSBColor(.3f, .2f, .8f);
	Color clrDataModeFile = Color.getHSBColor(.55f, .2f, .8f);
	private ZipController controller;

	JLabel lblHead;

	JLabel lblSearchFuzzy;
	JTextField txfSearchFuzzy;

	JLabel lblSearchZip;
	JTextField txfSearchZip;
	JLabel lblSearchCity;
	JTextField txfSearchCity;
	JLabel lblSearchState;
	JComboBox<String> cboxState;

	DefaultTableModel tmodResult;
	JTable tblResult;
	JScrollPane spResult;

	JMenuBar mb;

	JMenuItem miDummy;
	JMenuItem miDataStoreModeDB;
	JMenuItem miDataStoreModeFile;
	JMenuItem miSearchModeFuzzy;
	JMenuItem miSearchModeAdvanced;
	JMenuItem miHelp;

	JPanel pnlHead;
	JPanel pnlFuzzyBody;
	JPanel pnlAdvancedBody;
	JPanel pnlContent;

	JTextArea txaHelp;

	public ZipView() throws HeadlessException {
		super("Zip Code Master");
		this.controller = new ZipController(this, Mode.FILE);
		initComponents();
		if (controller.model instanceof ZipFileModel) {
			miDataStoreModeDB.setVisible(false);
			this.setBackground(clrDataModeFile);
		} else {
			miDataStoreModeFile.setVisible(false);
			this.setBackground(clrDataModeDB);
		}

		this.setJMenuBar(mb);
		mb.add(miDataStoreModeDB);
		mb.add(miDataStoreModeFile);
		mb.add(miSearchModeFuzzy);
		mb.add(miSearchModeAdvanced);
		mb.add(miHelp);

		pnlHead.add(lblHead);
		pnlContent.add(pnlHead);

		pnlFuzzyBody.add(lblSearchFuzzy);
		pnlFuzzyBody.add(txfSearchFuzzy);
		pnlContent.add(pnlFuzzyBody);

		pnlAdvancedBody.add(lblSearchZip);
		pnlAdvancedBody.add(txfSearchZip);
		pnlAdvancedBody.add(lblSearchCity);
		pnlAdvancedBody.add(txfSearchCity);
		pnlAdvancedBody.add(lblSearchState);
		pnlAdvancedBody.add(cboxState);

		pnlContent.add(pnlAdvancedBody);

		pnlContent.add(spResult);

		miDataStoreModeDB.addActionListener(controller);
		miDataStoreModeFile.addActionListener(controller);
		miHelp.addActionListener(controller);
		miSearchModeAdvanced.addActionListener(controller);
		miSearchModeFuzzy.addActionListener(controller);

		pnlFuzzyBody.addMouseListener(controller);
		txfSearchFuzzy.addKeyListener(controller);
		txfSearchFuzzy.addFocusListener(controller);

		txfSearchZip.addKeyListener(controller);
		txfSearchZip.addFocusListener(controller);
		txfSearchCity.addKeyListener(controller);
		cboxState.addActionListener(controller);

		pnlAdvancedBody.addMouseListener(controller);

		spResult.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		spResult.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

		pnlHead.setLayout(new FlowLayout(FlowLayout.CENTER, 140, 0));
		pnlFuzzyBody.setPreferredSize(new Dimension(350, 80));
		pnlFuzzyBody.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 30));
		pnlAdvancedBody.setPreferredSize(new Dimension(350, 80));
		pnlAdvancedBody.setLayout(new GridLayout(3, 1, 0, 10));

		spResult.setPreferredSize(new Dimension(550, 373));
		spResult.getViewport().setBackground(Color.WHITE);
		tblResult.getTableHeader().setBackground(Color.WHITE);
		pnlContent.setLayout(new FlowLayout(FlowLayout.CENTER, 2_000, 10));

		miSearchModeAdvanced.setVisible(false);
		pnlAdvancedBody.setVisible(false);

		mb.setOpaque(false);
		miSearchModeFuzzy.setOpaque(false);
		miSearchModeAdvanced.setOpaque(false);
		miHelp.setOpaque(false);
		pnlHead.setOpaque(false);
		pnlFuzzyBody.setOpaque(false);
		pnlAdvancedBody.setOpaque(false);
		spResult.setOpaque(false);
		pnlContent.setOpaque(false);

		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setContentPane(pnlContent);
		this.setSize(570, 559);
		this.setLocationRelativeTo(null);

	}

	private void initComponents() {

		mb = new JMenuBar();

		miDataStoreModeDB = new JMenuItem(String.format("%-15s", "Datenbankmodus"));
		miDataStoreModeDB.setFont(font);
		miDataStoreModeDB.setBackground(clrDataModeDB);
		miDataStoreModeDB.setToolTipText("Mausklick oder \"Strg+D\" f�r Dateimodus");
		miDataStoreModeDB.setMnemonic(KeyEvent.VK_D);
		miDataStoreModeDB.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_DOWN_MASK));

		miDataStoreModeFile = new JMenuItem(String.format("%-15s", "Dateimodus"));
		miDataStoreModeFile.setFont(font);
		miDataStoreModeFile.setBackground(clrDataModeFile);
		miDataStoreModeFile.setToolTipText("Mausklick oder \"Strg+D\" f�r Datenbankmodus");
		miDataStoreModeFile.setMnemonic(KeyEvent.VK_D);
		miDataStoreModeFile.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_DOWN_MASK));

		miSearchModeFuzzy = new JMenuItem(String.format("%-20s", "Einfache Suche"));
		miSearchModeFuzzy.setFont(font);
		miSearchModeFuzzy.setToolTipText("Mausklick oder \"Strg+S\" f�r erweiterte Suche");
		miSearchModeFuzzy.setMnemonic(KeyEvent.VK_S);
		miSearchModeFuzzy.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));

		miSearchModeAdvanced = new JMenuItem(String.format("%-20s", "Erweiterte Suche"));
		miSearchModeAdvanced.setFont(font);
		miSearchModeAdvanced.setToolTipText("Mausklick oder \"Strg+S\" f�r einfache Suche");
		miSearchModeAdvanced.setMnemonic(KeyEvent.VK_S);
		miSearchModeAdvanced.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));

		miHelp = new JMenuItem(String.format("%-8s", "Hilfe"));
		miHelp.setFont(font);
		miHelp.setToolTipText("Mausklick oder \"Alt+H\" f�r Hilfe");
		miHelp.setMnemonic(KeyEvent.VK_H);
		miHelp.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_H, InputEvent.ALT_DOWN_MASK));
		miHelp.setHorizontalAlignment(SwingConstants.TRAILING);

		lblHead = new JLabel("Zip Code Master");
		lblHead.setFont(font);
		pnlHead = new JPanel();

		lblSearchFuzzy = new JLabel("Freie Suche");
		lblSearchFuzzy.setFont(font);
		txfSearchFuzzy = new JTextField(30);
		txfSearchFuzzy.setFont(font);
		pnlFuzzyBody = new JPanel();
		pnlFuzzyBody.setToolTipText("Mausklick f�r erweiterte Suche");

		lblSearchZip = new JLabel("PLZ");
		lblSearchZip.setFont(font);
		txfSearchZip = new JTextField(30);
		txfSearchZip.setFont(font);
		lblSearchCity = new JLabel("Ort");
		lblSearchCity.setFont(font);
		txfSearchCity = new JTextField(30);
		txfSearchCity.setFont(font);
		lblSearchState = new JLabel("Bundesland");
		lblSearchState.setFont(font);
		cboxState = new JComboBox<String>();
		cboxState.setFont(font);
		cboxState.addItem("Alle");
		for (String item : controller.model.getAllStates())
			cboxState.addItem(item);
		cboxState.setBackground(Color.WHITE);
		pnlAdvancedBody = new JPanel();
		pnlAdvancedBody.setToolTipText("Mausklick f�r freie Suche");

		tmodResult = new DefaultTableModel();
		tmodResult.addColumn("Postleitzahl");
		tmodResult.addColumn("Stadt");
		tmodResult.addColumn("Bundesland");
		tblResult = new JTable(tmodResult);
		tblResult.getColumnModel().getColumn(0).setMaxWidth(100);
		tblResult.getColumnModel().getColumn(2).setMaxWidth(170);
		tblResult.getColumnModel().getColumn(2).setMinWidth(170);

		tblResult.setFont(font);
		tblResult.setEnabled(false);
		spResult = new JScrollPane(tblResult);

		pnlContent = new JPanel();
		txaHelp = new JTextArea(10, 2);
		txaHelp.setFont(font);
		txaHelp.setOpaque(false);
		txaHelp.append(String.format("%1$-8s\t %2$-25s", "Alt+H", "Dieses Hilfemen� anzeigen"));
		txaHelp.append(System.lineSeparator());
		txaHelp.append(String.format("%1$-8s\t %2$-25s", "Strg+D", "Datenmodus wechseln"));
		txaHelp.append(System.lineSeparator());
		txaHelp.append(String.format("%1$-8s\t %2$-25s", "Strg+S", "Suchmodus wechseln"));
		txaHelp.append(System.lineSeparator());
		txaHelp.append(String.format("%1$-8s\t %2$-25s", "Alt+F4", "Programm beenden"));
		txaHelp.append(System.lineSeparator());
		txaHelp.append(System.lineSeparator());
		txaHelp.append(System.lineSeparator());
		txaHelp.append("Zip Code Master");
		txaHelp.append(System.lineSeparator());
		txaHelp.append("Baldur Kellner");

	}
}
