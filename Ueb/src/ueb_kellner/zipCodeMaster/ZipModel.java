package ueb_kellner.zipCodeMaster;

import java.util.ArrayList;

public abstract class ZipModel {

	enum Mode {
		FILE, DB
	};

	abstract ArrayList<String[]> fuzzySearch(String zip);

	abstract ArrayList<String[]> advancedSearch(String searchTerm[]);

	abstract ArrayList<String> getAllStates();

	static ZipModel getInstance(Mode modus) {
		if (modus == Mode.FILE) {
			return new ZipFileModel();
		} else if (modus == Mode.DB) {
			return new ZipDbModel();
		}
		return null;
	}
}
