package ueb_kellner.zipCodeMaster;

/**
 * @author kerrnelPanic
 *
 */
public final class ZipLocation implements Comparable<ZipLocation> {

	final String zip;
	final String city;
	final String state;

	private ZipLocation(String zip, String city, String state) {
		this.zip = zip;
		this.city = city;
		this.state = state;
	}

	static ZipLocation of(String zip, String city, String state) {
		return new ZipLocation(zip, city, state);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((zip == null) ? 0 : zip.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ZipLocation other = (ZipLocation) obj;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (zip == null) {
			if (other.zip != null)
				return false;
		} else if (!zip.equals(other.zip))
			return false;
		return true;
	}

//	@Override
//	public int compare(ZipLocation o1, ZipLocation o2) {
//		return o1.zip.concat(o1.city).concat(o1.state).compareTo(o2.zip.concat(o2.city).concat(o2.state));
//	}

	@Override
	public String toString() {
		return "ZipLocation [zip=" + zip + ", city=" + city + ", state=" + state + "]";
	}

	public String[] toStringArray() {
		return new String[] { this.zip, this.city, this.state };
	}

	@Override
	public int compareTo(ZipLocation o) {
		if (zip.equals(o.zip)) {
			if (city.equals(o.city)) {
				return state.compareTo(o.state);
			} else {
				return city.compareTo(o.city);
			}
		} else {
			return zip.compareTo(o.zip);
		}
	}

}
