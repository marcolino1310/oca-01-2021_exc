package ueb_boettger;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.*;

public class ZinsrechnerView extends JFrame{
	private static final long serialVersionUID = 1L;
	
//	Deklaration der Komponenten
//	Men�leiste
	JMenuBar mnbMenu;
	JMenu mnuDatei;
	JMenu mnuInfo;
	JMenuItem mniToggleMethode;
	JMenuItem mniBerechnen;
	JMenuItem mniKopieren;
	JMenuItem mniLoeschen;
	JMenuItem mniBeenden;
	JMenuItem mniInfo;
//	Bezeichnungsfelder/Labels
	JLabel lblStartkapital;
	JLabel lblZinssatz;
	JLabel lblLaufzeit;
	JLabel lblEndkapital;
	JLabel lblTitel;
//	Textfelder/textfields
	JTextField txfStartkapital;
	JTextField txfZinssatz;
	JTextField txfLaufzeit;
	JTextField txfEndkapital;
//	Befehls-Schaltfl�chen/Buttons
	JButton btnBerechnen;
	JButton btnLoeschen;
	JButton btnBeenden;
	JButton btnKopieren;
//	Optionsfelder/Radio buttons
	JRadioButton rbtEndkapital;
	JRadioButton rbtLaufzeit;
//	ButtonGroup zum gruppieren von Optionsfelder (-> genau eins ausgew�hlt)
	ButtonGroup btgModus;
//	Erweiterung: Jahresverlauf
	JTextArea txaVerlauf;
	JScrollPane scpVerlauf;
	
//	Container f�r Komponenten (JPanel)
	JPanel pnlInhalt;
	JPanel pnlButtons;
	JPanel pnlButtons2;
	JPanel pnlTitel;
	JPanel pnlGesamt;
	
//	Controller als Member-Variable
	ZinsrechnerController controller = new ZinsrechnerController(this);

	public ZinsrechnerView() {
		super("Zinsrechner");							// Titelleiste
//		this.setTitle("Zinsrechner");					// Alternative zu oberem
		initComponents();
//		Hinzuf�gen der Komponenten zum Container
		mnbMenu.add(mnuDatei);
		mnuDatei.setMnemonic('D');
		mnbMenu.add(mnuInfo);
		mnuInfo.setMnemonic('I');
		mnuDatei.add(mniToggleMethode);
		mniToggleMethode.setMnemonic('W');
		mnuDatei.add(mniBerechnen);
		mniBerechnen.setMnemonic('B');
		mnuDatei.add(mniLoeschen);
		mniLoeschen.setMnemonic('L');
		mnuDatei.add(mniKopieren);
		mniKopieren.setMnemonic('K');
		mnuDatei.add(mniBeenden);
		mniBeenden.setMnemonic('E');
		mnuInfo.add(mniInfo);
		mniInfo.setMnemonic('I');
		this.setJMenuBar(mnbMenu);
		
		pnlTitel.add(lblTitel);
		pnlInhalt.add(lblStartkapital);
		pnlInhalt.add(txfStartkapital);
		pnlInhalt.add(lblZinssatz);
		pnlInhalt.add(txfZinssatz);
		pnlInhalt.add(lblLaufzeit);
		pnlInhalt.add(txfLaufzeit);
		pnlInhalt.add(lblEndkapital);
		pnlInhalt.add(txfEndkapital);
		txfEndkapital.setEditable(false);
		pnlInhalt.add(rbtEndkapital);
		rbtEndkapital.setSelected(true);
		pnlInhalt.add(rbtLaufzeit);
		btgModus.add(rbtEndkapital);
		btgModus.add(rbtLaufzeit);
		pnlButtons.add(btnBerechnen);
		pnlButtons.add(btnLoeschen);
		pnlButtons.add(btnBeenden);
		pnlButtons2.add(btnKopieren);
//		Panel add
		pnlGesamt.add(pnlTitel);
		pnlGesamt.add(pnlInhalt);
		pnlGesamt.add(pnlButtons);
		pnlGesamt.add(scpVerlauf);
		pnlGesamt.add(pnlButtons2);
//		WindowListener f�r den JFrame
		this.addWindowListener(controller);
//		KeyListener:
		txfStartkapital.addKeyListener(controller);
		txfZinssatz.addKeyListener(controller);
		txfLaufzeit.addKeyListener(controller);
		txfEndkapital.addKeyListener(controller);
//		ActionListener f�r den Button Beenden
		rbtEndkapital.addActionListener(controller);
		rbtLaufzeit.addActionListener(controller);
		mniToggleMethode.addActionListener(controller);
		btnBerechnen.addActionListener(controller);
		mniBerechnen.addActionListener(controller);
		btnLoeschen.addActionListener(controller);
		mniLoeschen.addActionListener(controller);
		btnKopieren.addActionListener(controller);
		mniKopieren.addActionListener(controller);
		btnBeenden.addActionListener(controller);
		mniBeenden.addActionListener(controller);
		mniInfo.addActionListener(controller);
//		ActionListener f�r den Button Beenden ohne Model-View-Controller
//		btnBeenden.addActionListener(e -> System.exit(ABORT));
//		btnBeenden.addActionListener(new ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				System.exit(ABORT);
//			}
//		});
		
//		Layout des Panels setzen (normal: Flow-Layout), hier Grid-Layout mit 5 Zeilen, 2 Spalten, 30 Abst�nde
		pnlInhalt.setLayout(new GridLayout(6, 2, 20, 5));
		scpVerlauf.setPreferredSize(new Dimension(200, 170));
		scpVerlauf.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		txaVerlauf.setFont(new Font("Courier New", Font.PLAIN, 12));
		pnlGesamt.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
//		Setzen des Panels als Inhaltsbereich
		this.setContentPane(pnlGesamt);
		this.getContentPane().setBackground(Color.decode("#EEDD82"));
		pnlTitel.setBackground(Color.decode("#EEDD82"));
		pnlInhalt.setBackground(Color.decode("#EEDD82"));
		scpVerlauf.setBackground(Color.decode("#EEDD82"));
		rbtEndkapital.setBackground(Color.decode("#EEDD82"));
		rbtLaufzeit.setBackground(Color.decode("#EEDD82"));
		pnlButtons.setBackground(Color.decode("#EEDD82"));
		pnlButtons2.setBackground(Color.decode("#EEDD82"));
		this.setSize(300, 560);							// Breite mal H�he
		this.setLocationRelativeTo(null);				// Fenster wird mittig angeordnet
//		Beim schlie�en wird jetzt nichts mehr ver�ndert, da WindowListener aktiv ist
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.setVisible(true);							// Fenster wird sichtbar gemacht
	}
	
	private void initComponents() {
		mnbMenu = new JMenuBar();
		mnuDatei = new JMenu("Datei");
		mnuInfo = new JMenu("Info");
		mniToggleMethode = new JMenuItem("Wechsel Endkapital/Laufzeit");
		mniBerechnen = new JMenuItem("Berechnen");
		mniKopieren = new JMenuItem("Kopieren");
		mniLoeschen = new JMenuItem("L�schen");
		mniBeenden = new JMenuItem("Beenden");
		mniInfo = new JMenuItem("Info");
		
		lblTitel =new JLabel("Zinsrechner");
		lblStartkapital = new JLabel("Startkapital");
		lblZinssatz = new JLabel("Zinssatz");
		lblLaufzeit = new JLabel("Laufzeit");
		lblEndkapital = new JLabel("Endkapital");
		
		txfStartkapital = new JTextField(10);
		txfZinssatz = new JTextField(10);
		txfLaufzeit = new JTextField(10);
		txfEndkapital = new JTextField(10);
		
		rbtEndkapital = new JRadioButton("Endkapital");
		rbtLaufzeit = new JRadioButton("Laufzeit");
		btgModus = new ButtonGroup();
		
		btnBerechnen = new JButton("Berechnen");
		btnLoeschen = new JButton("L�schen");
		btnBeenden = new JButton("Beenden");
		btnKopieren = new JButton("Kopieren");
		
		txaVerlauf = new JTextArea(10, 2);
		scpVerlauf = new JScrollPane(txaVerlauf);
		
		pnlInhalt = new JPanel();
		pnlButtons = new JPanel();
		pnlButtons2 = new JPanel();
		pnlTitel = new JPanel();
		pnlGesamt = new JPanel();
	}
}
