package ueb_boettger.postcodeSearch;

public class PLZRun {

	public static void main(String[] args) {
		PLZModel model = new PLZDBModel();
		String plz = "03172";
		String place = model.getPlace(plz);
		System.out.println("Ort: " + place);
		String state = model.getState(plz);
		System.out.println("Bundesland: " + state);
	}
}
