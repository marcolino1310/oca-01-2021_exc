package ueb_boettger.postcodeSearch;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class PLZDBModel implements PLZModel{

//	um verschiedene Datentypen zu benutzen, nur accessConnect anpassen, z.B. jdbc:sql/jdbc:mysql
	private String accessConnect = "jdbc:ucanaccess://files/PLZ_Datenbank.accdb";
	private Connection connection;
	private Statement command;
	
	PLZDBModel() {
//		Treiber laden, seit 1.7 nicht mehr zwingend (nur f�r Kontrollzwecke)
		try {
			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(accessConnect);
			System.out.println("Verbindung erfolgreich");
			command = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getPlace(String postcode) {
		boolean flag = false;
		String ret = null;
		try {
//			query ~ Auswahl
			ResultSet rs = command.executeQuery("select Ort from tblPLZ where PLZ = " + postcode);
			while(rs.next()) {
				if(flag) {
					ret += ", " + rs.getString("Ort");
				} else {
					flag = true;
					ret = rs.getString("Ort");
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ret;
	}

	@Override
	public String getState(String postcode) {
		boolean flag = false;
		String ret = null;
		try {
//			query ~ Auswahl
			ResultSet rs = command.executeQuery("select Bundesland from tblPLZ where PLZ = " + postcode);
			while(rs.next()) {
				if(flag) {
					ret += ", " + rs.getString("Bundesland");
				} else {
					flag = true;
					ret = rs.getString("Bundesland");
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ret;
	}

}
