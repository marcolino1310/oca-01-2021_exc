package ueb_boettger.postcodeSearch;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class PLZFileModel implements PLZModel{
	private ArrayList<String> lines = new ArrayList<>();
	private String postcodeFile = "files/plz.txt";
	
	public PLZFileModel() {
		init();
	}
	
	@Override
	public String getPlace(String postcode) {
		return getLine(postcode, 1);
	}	
	@Override
	public String getState(String postcode) {
		return getLine(postcode, 5);
	}
	private String getLine(String postcode, int pos) {
		for (String line : lines) {
			if (line.startsWith(postcode))
				return line.split("\\t")[pos];
		}
		return null;
	}
	
	private void init() {
//		try-with-resources
		try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(postcodeFile), 
					"UTF8"))) {
			String line;
			while((line = br.readLine()) != null)
				lines.add(line);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unused")
	private void initPre7() {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(postcodeFile), 
					"UTF8"));
			String line;
			while((line = br.readLine()) != null)
				lines.add(line);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
//	@Override
//	public String getPlace(String postcode) { 
//		String result = "";
//		try {
//			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("files/plz.txt"), 
//					"UTF8"));
//			String line;
//			outer: while(true) {
//				while((line = br.readLine()) != null) {
//					String[] values = line.split("\t");
//					if(values[0].equals(postcode)) {
//						result = values[1];
//						break outer;
//					}
//				}
//				br.close();
//				return "PLZ nicht gefunden.";
//			}
//			br.close();
//		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return result;
//	}
//
//	@Override
//	public String getState(String postcode) {
//		String result = "";
//		try {
//			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("files/plz.txt"), 
//					"UTF8"));
//			String line;
//			outer: while(true) {
//				while((line = br.readLine()) != null) {
//					String[] values = line.split("\t");
//					if(values[0].equals(postcode)) {
//						result = values[5];
//						break outer;
//					}
//				}
//				br.close();
//				return "PLZ nicht gefunden.";
//			}
//			br.close();
//		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return result;
//	}
}
