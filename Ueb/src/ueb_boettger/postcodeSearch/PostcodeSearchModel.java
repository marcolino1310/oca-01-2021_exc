package ueb_boettger.postcodeSearch;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class PostcodeSearchModel {
	
//	implementieren von Constructor mit BR und ArrayList
	
	private enum searchModus {BYPLACE(0), BYPOSTCODE(1);
		private final int i;
		private searchModus(int i) {
			this.i = i;
		}
		public int getValue() {
			return i;
		}
		}
	
	protected String[] searchByPostcode(String postcode) {
		return getLines(postcode, searchModus.BYPOSTCODE.getValue());
	}
	protected String[] searchByPlace(String locality) {
		return getLines(locality, searchModus.BYPLACE.getValue());
	}
	private String[] getLines(String input, int pos) {
		int pos1, pos2;
		if(pos == 0) {
			pos1 = 1;
			pos2 = 0;
		}
		else {
			pos1 = 0;
			pos2 = 1;
		}
		String[] result = new String[3];
		try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("files/plz.txt"), 
				"UTF8"))) {
			String line;
			boolean flag = false;
			outer: while(true) {
				while((line = br.readLine()) != null) {
					String[] values = line.split("\t");
					if(values[pos1].equals(input)) {
						if(!flag) {
							result[0] = values[pos2];
							result[1] = values[3];
							result[2] = values[5];
						}
						else {
							result[0] = result[0] + ";" + values[pos2];
						}
						flag = true;
					}
					if(flag && !values[1].equals(input))
						break outer;
				}
				result[0] = "Ort nicht gefunden.";
				result[1] = "";
				result[2] = "";
				break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
}
