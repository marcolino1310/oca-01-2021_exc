package ueb_boettger.postcodeSearch;

public interface PLZModel {
	
	enum Modus{FILE, DB}

	String getPlace(String postcode);
	
	String getState(String postcode);
	
	static PLZModel getInstance(Modus modus) {
		if (modus == Modus.FILE) {
			return new PLZFileModel();
		} else if (modus == Modus.DB) {
			return new PLZDBModel();
		}
		return null;
	}
}
