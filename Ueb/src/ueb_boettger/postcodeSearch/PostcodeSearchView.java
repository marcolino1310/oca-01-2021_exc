package ueb_boettger.postcodeSearch;

import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class PostcodeSearchView extends JFrame{

	private static final long serialVersionUID = 1L;

	JMenuBar mnbMenu;
	JMenu mnuFile;
	JMenu mnuHelp;
	JMenuItem mniSearch;
	JMenuItem mniToggleSearchModus;
	JMenuItem mniCopy;
	JMenuItem mniClear;
	JMenuItem mniExit;
	JMenuItem mniInfo;
	
	JPanel pnlTotal;
	JPanel pnlHeader;
	JPanel pnlContent;
	JLabel lblHeader;
	JLabel lblPostcode;
	JLabel lblPlace;
	JLabel lblCounty;
	JLabel lblState;
	JTextField txfPostcode;
	JTextField txfPlace;
	JTextField txfCounty;
	JTextField txfState;
	JRadioButton rbtSearchByPostcode;
	JRadioButton rbtSearchByPlace;
	JButton btnSearch;
	JButton btnCopy;
	JButton btnClear;
	JButton btnExit;
	ButtonGroup btgModus;
	
	PostcodeSearchController controller = new PostcodeSearchController(this);
	
	public PostcodeSearchView() {
		super("PLZ Suche");
		initComponents();
		mnbMenu.add(mnuFile);
		mnbMenu.add(mnuHelp);
		mnuFile.add(mniSearch);
		mnuFile.add(mniToggleSearchModus);
		mnuFile.add(mniCopy);
		mnuFile.add(mniClear);
		mnuFile.add(mniExit);
		mnuHelp.add(mniInfo);
		pnlTotal.add(pnlHeader);
		pnlTotal.add(pnlContent);
		pnlHeader.add(lblHeader);
		pnlContent.add(lblPostcode);
		pnlContent.add(txfPostcode);
		pnlContent.add(lblPlace);
		pnlContent.add(txfPlace);
		pnlContent.add(lblCounty);
		pnlContent.add(txfCounty);
		pnlContent.add(lblState);
		pnlContent.add(txfState);
		pnlContent.add(rbtSearchByPlace);
		pnlContent.add(rbtSearchByPostcode);
		pnlContent.add(btnSearch);
		pnlContent.add(btnCopy);
		pnlContent.add(btnClear);
		pnlContent.add(btnExit);
		btgModus.add(rbtSearchByPlace);
		btgModus.add(rbtSearchByPostcode);
		
		mnuFile.setMnemonic('D');
		mnuHelp.setMnemonic('H');
		txfPostcode.setEditable(false);
		txfCounty.setEditable(false);
		txfState.setEditable(false);
		
		this.addWindowListener(controller);
		mniSearch.addActionListener(controller);
		mniToggleSearchModus.addActionListener(controller);
		mniCopy.addActionListener(controller);
		mniClear.addActionListener(controller);
		mniExit.addActionListener(controller);
		mniInfo.addActionListener(controller);
		rbtSearchByPlace.addActionListener(controller);
		rbtSearchByPostcode.addActionListener(controller);
		btnSearch.addActionListener(controller);
		btnCopy.addActionListener(controller);
		btnClear.addActionListener(controller);
		btnExit.addActionListener(controller);
		txfPostcode.addKeyListener(controller);
		txfPlace.addKeyListener(controller);
		
		this.setJMenuBar(mnbMenu);
		pnlTotal.setLayout(new FlowLayout(FlowLayout.CENTER, 2000, 10));
		pnlContent.setLayout(new GridLayout(9, 2, 0, 10));
		this.setContentPane(pnlTotal);
		this.setSize(380, 365);	
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.setVisible(true);
	}
	
	private void initComponents() {
		mnbMenu = new JMenuBar();
		mnuFile = new JMenu("Datei");
		mnuHelp = new JMenu("Hilfe");
		mniSearch = new JMenuItem("Suchen", 'S');
		mniToggleSearchModus = new JMenuItem("Suchmodus wechseln", 'W');
		mniCopy = new JMenuItem("Kopieren", 'K');
		mniClear = new JMenuItem("L�schen", 'L');
		mniExit = new JMenuItem("Exit", 'E');
		mniInfo = new JMenuItem("Info", 'I');
		pnlTotal = new JPanel();
		pnlHeader = new JPanel();
		pnlContent = new JPanel();
		lblHeader = new JLabel("PLZ Suche");
		lblPostcode = new JLabel("PLZ");
		lblPlace = new JLabel("Ort");
		lblCounty = new JLabel("Kreis");
		lblState = new JLabel("Bundesland");
		txfPostcode = new JTextField(15);
		txfPlace = new JTextField(15);
		txfCounty = new JTextField(15);
		txfState = new JTextField(15);
		rbtSearchByPlace = new JRadioButton("Suche �ber Ort", true);
		rbtSearchByPostcode = new JRadioButton("Suche �ber PLZ");
		btgModus = new ButtonGroup();
		btnSearch = new JButton("Suchen");
		btnCopy = new JButton("Kopieren");
		btnClear = new JButton("L�schen");
		btnExit = new JButton("Beenden");
	}
}
