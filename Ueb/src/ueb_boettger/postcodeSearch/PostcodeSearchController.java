package ueb_boettger.postcodeSearch;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import javax.swing.JOptionPane;

public class PostcodeSearchController extends PostcodeSearchAdapter{

	PostcodeSearchView view;
	PostcodeSearchModel model;
	
	public PostcodeSearchController(PostcodeSearchView view) {
		this.view = view;
		model = new PostcodeSearchModel();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == view.btnSearch || e.getSource() == view.mniSearch) {
			search();
		} else if (e.getSource() == view.mniToggleSearchModus) {
			if (view.rbtSearchByPlace.isSelected()) {
				view.rbtSearchByPostcode.setSelected(true);
				view.txfPlace.setEditable(false);
				view.txfPostcode.setEditable(true);
				clear();
			} else {
				view.rbtSearchByPlace.setSelected(true);
				view.txfPlace.setEditable(true);
				view.txfPostcode.setEditable(false);
				clear();
			}
		} else if (e.getSource() == view.btnCopy || e.getSource() == view.mniCopy)
			copy();
		else if (e.getSource() == view.btnClear || e.getSource() == view.mniClear)
			clear();
		else if (e.getSource() == view.btnExit || e.getSource() == view.mniExit) {
			exit();
		} else if (e.getSource() == view.mniInfo)
			JOptionPane.showMessageDialog(null, "PostcodeSearch 2021 @SeppReifen", "Info", 
					JOptionPane.INFORMATION_MESSAGE);
		else if (e.getSource() == view.rbtSearchByPlace) {
			view.txfPlace.setEditable(true);
			view.txfPostcode.setEditable(false);
			clear();
		} else if (e.getSource() == view.rbtSearchByPostcode ) {
			view.txfPlace.setEditable(false);
			view.txfPostcode.setEditable(true);
			clear();
		}
	}

	@Override
	public void windowClosing(WindowEvent e) {
		exit();
	}
	@Override
	public void keyReleased(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_ENTER)
			search();
	}
	
	private void search() {
		String[] result = new String[3];
		if(view.rbtSearchByPlace.isSelected()) {
			if(view.txfPlace.getText().chars().allMatch(Character::isSpaceChar)) {
				JOptionPane.showMessageDialog(null, "Ungültiger Ortsname", "Ungültiger Ortsname", 
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			for (int i = 0; i < view.txfPlace.getText().length(); i++) {
				if(!Character.isAlphabetic(view.txfPlace.getText().charAt(i)) 
						&& view.txfPlace.getText().charAt(i) != ' ' ) {
					JOptionPane.showMessageDialog(null, "Ungültiger Ortsname", "Ungültiger Ortsname", 
							JOptionPane.ERROR_MESSAGE);
					return;
				}
			}
			result = model.searchByPlace(view.txfPlace.getText());
			String[] values = result[0].split(";");
			if(values.length > 1)
				result[0] = values[0] + "-" + values[values.length-1];
			view.txfPostcode.setText(result[0]);
			view.txfCounty.setText(result[1]);
			view.txfState.setText(result[2]);
		}
		else {
			if(view.txfPostcode.getText().isEmpty() && view.txfPlace.getText().isEmpty())
				return;
			if(!view.txfPostcode.getText().chars().allMatch(Character::isDigit)) {
				JOptionPane.showMessageDialog(null, "Ungültige PLZ", "Ungültige PLZ", 
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			result = model.searchByPostcode(view.txfPostcode.getText());
			view.txfPlace.setText(result[0]);
			view.txfCounty.setText(result[1]);
			view.txfState.setText(result[2]);
		}
	}
	private void copy() {
		String sep = System.lineSeparator();
		StringSelection mySelection = new StringSelection(view.txfPlace.getText() + sep 
				+ view.txfPostcode.getText() + sep + view.txfCounty + sep + view.txfState);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(mySelection, null);
	}
	private void clear() {
		view.txfPostcode.setText("");
		view.txfPlace.setText("");
		view.txfCounty.setText("");
		view.txfState.setText("");
	}
	private void exit() {
		int antwort = JOptionPane.showConfirmDialog(view, "Sind Sie sich sicher?", "Beenden", JOptionPane.YES_NO_OPTION);
		if(antwort == JOptionPane.YES_OPTION)
			System.exit(0);
	}

}
