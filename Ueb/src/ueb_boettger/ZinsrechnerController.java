package ueb_boettger;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.NumberFormat;
import javax.swing.JOptionPane;

/**
 * WindowAdapter: um WindowListener zu implementieren, ohne alle abstrakten Methoden zu implementieren
 * 	- Nachteil: man kann nur eine Adapter-Klasse erben
 * - m�glich auch: eigene Adapter-Klasse schreiben, die alle Interfaces implementiert, die man braucht
 *
 */
public class ZinsrechnerController extends WindowAdapter implements ActionListener, KeyListener{

	ZinsrechnerView view;
	ZinsrechnerModel model;
	
	public ZinsrechnerController(ZinsrechnerView view) {
		this.view = view;
		this.model = new ZinsrechnerModel();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == view.btnBerechnen || e.getSource() == view.mniBerechnen)
			berechnen();
		else if(e.getSource() == view.mniToggleMethode) {
			if(view.rbtEndkapital.isSelected()) {
				view.rbtLaufzeit.setSelected(true);
				view.txfLaufzeit.setEditable(false);
				view.txfEndkapital.setEditable(true);
				loeschen();
			}
			else {
				view.rbtEndkapital.setSelected(true);
				view.txfEndkapital.setEditable(false);
				view.txfLaufzeit.setEditable(true);
				loeschen();
			}
		}
		else if(e.getSource() == view.btnLoeschen || e.getSource() == view.mniLoeschen)
			loeschen();
		else if(e.getSource() == view.btnKopieren || e.getSource() == view.mniKopieren) {
			String kopierText = view.txaVerlauf.getText();
			StringSelection mySelection = new StringSelection(kopierText);
			Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
			clipboard.setContents(mySelection, null);
		}
		else if(e.getSource() == view.btnBeenden || e.getSource() == view.mniBeenden)
			beenden();
		else if(e.getSource() == view.mniInfo)
			JOptionPane.showMessageDialog(null, "Zinsrechner 1.0", "Info", JOptionPane.INFORMATION_MESSAGE);
		else if(e.getSource() == view.rbtEndkapital) {
			view.txfEndkapital.setEditable(false);
			view.txfLaufzeit.setEditable(true);
			loeschen();
		}
		else if(e.getSource() == view.rbtLaufzeit) {
			view.txfLaufzeit.setEditable(false);
			view.txfEndkapital.setEditable(true);
			loeschen();
		}
	}

	private void berechnen() {
		view.txaVerlauf.setText("");
		int startkapital = Integer.valueOf(view.txfStartkapital.getText());
		double zinssatz = Double.valueOf(view.txfZinssatz.getText());
		double laufzeit;
		if(view.rbtEndkapital.isSelected()) {
			laufzeit = Integer.valueOf(view.txfLaufzeit.getText());
//			Aufruf der berechneEndkapital-Methode aus dem Model
			double endkapital = model.berechneEndkapital(startkapital, zinssatz, (int)laufzeit);	
//			String endkapitalString = String.format("%,.2f �", endkapital);
//			Gleiches Ergebnis wie obere, aber W�hreung entsprich lokaler W�hrung
			String endkapitalString = NumberFormat.getCurrencyInstance().format(endkapital);
//			Ergebnis in Textfeld eintragen (setText)
			view.txfEndkapital.setText(endkapitalString);
		}
		else {
			int endkapital = Integer.valueOf(view.txfEndkapital.getText());
//			Aufruf der berechneLaufzeit-Methode aus dem Model und Formattierung
			laufzeit = model.berechneLaufzeit(startkapital, zinssatz, endkapital);
			String laufzeitString = String.format("%,.2f", laufzeit);
//			Ergebnis in Textfeld eintragen (setText)
			view.txfLaufzeit.setText(laufzeitString);
		}
		ausgeben(startkapital, zinssatz, laufzeit);
	}
	private void ausgeben(int startkapital, double zinssatz, double laufzeit) {
		laufzeit = Math.ceil(laufzeit);
		for (int jahr = 1; jahr <= laufzeit; jahr++) {
			view.txaVerlauf.append(String.format("%2d\t%,15.2f �%n", jahr, 
					model.berechneEndkapital(startkapital, zinssatz, jahr)));
//			double kapital = model.berechneEndkapital(startkapital, zinssatz, jahr);
//			view.txaVerlauf.append((i+1) + "\t" + NumberFormat.getCurrencyInstance().format(kapital) + "\n");
		}
	}

	private void loeschen() {
		view.txfStartkapital.setText("");
		view.txfZinssatz.setText("");
		view.txfLaufzeit.setText("");
		view.txfEndkapital.setText("");
		view.txaVerlauf.setText("");
		view.txfStartkapital.requestFocus();
	}
	
	private void beenden() {
		int antwort = JOptionPane.showConfirmDialog(view, "Sind Sie sich sicher?", "Beenden", JOptionPane.YES_NO_OPTION);
		if(antwort == JOptionPane.YES_OPTION)
			System.exit(0);
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}
	@Override
	public void keyPressed(KeyEvent e) {
	}
	@Override
	public void keyReleased(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_ENTER) {
			if(e.getSource() == view.txfStartkapital)
				view.txfZinssatz.requestFocus();
			else if(e.getSource() == view.txfZinssatz) {
				if(view.rbtEndkapital.isSelected())
					view.txfLaufzeit.requestFocus();
				else 
					view.txfEndkapital.requestFocus();
			}
			else
				berechnen();
		}
	}
	@Override
	public void windowClosing(WindowEvent e) {
		beenden();
	}

}
