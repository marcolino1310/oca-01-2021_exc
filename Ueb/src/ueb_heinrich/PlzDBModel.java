package ueb_heinrich;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;



public class PlzDBModel implements PlzModel{
	
	private String accessConnect="jdbc:ucanaccess://files/PLZ_Datenbank.accdb";
	private Connection verbindung;
	private Statement befehl;	
	
	public PlzDBModel() {
		try {
			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			verbindung=DriverManager.getConnection(accessConnect);
			befehl=verbindung.createStatement();
			System.out.println("Verbindung erfolgreich.");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public String getOrt(String plz) {
		String ergebnis="Ergebnis ";
		try {
			ResultSet rs=befehl.executeQuery("select Ort from tblPLZ where PLZ=plz");
			while(rs.next()) {
				//System.out.println(rs.getString("Ort"));
				ergebnis+=rs.getString("Ort")+"\n";
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ergebnis;
	}

	@Override
	public String getBundesland(String plz) {
		
		return null;
	}

}
