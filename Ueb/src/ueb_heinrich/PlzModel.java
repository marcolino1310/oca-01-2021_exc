package ueb_heinrich;

public interface PlzModel {
	
	String getOrt(String plz);
	String getBundesland(String plz);

}
