package ueb_heinrich;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class PlzFileModel implements PlzModel{
	
	private ArrayList<String> zeilen=new ArrayList();
	private String plzDatei="files\\plz.txt";

	
	
	public PlzFileModel() {
		super();
		init();
	}

	@Override
	public String getOrt(String plz) {	
		String ort="";
			for(int i=0; i<zeilen.size(); i++) {
				if(zeilen.get(i).split("\\t")[0].equals(plz))ort+=zeilen.get(i).split("\\t")[1];				
			}
			return ort;
	}

	@Override
	public String getBundesland(String plz) {
		String bundesland="";
		for(int i=0; i<zeilen.size(); i++) {
			int a=zeilen.get(i).length();
			if(zeilen.get(i).split("\\t")[0].equals(plz))bundesland+=zeilen.get(i).split("\\t")[a-1];				
		}
		return bundesland;
		
	}
	
	public void init() {
		String zeile=null;
		//try with ressources
		try(FileInputStream myFileInputStream=new FileInputStream(new File(plzDatei));
			InputStreamReader myInputStreamReader=new InputStreamReader(myFileInputStream,"UTF8");
			BufferedReader myBufferedReader=new BufferedReader(myInputStreamReader);)
		{
		StringTokenizer myStringTokenizer=null;
				while((zeile=myBufferedReader.readLine())!=null) {
					zeilen.add(zeile);
			}	myBufferedReader.close();
				myInputStreamReader.close();
				myFileInputStream.close();				
				
		} catch (Exception e) {			
			e.printStackTrace();
		}
					
	}


}
