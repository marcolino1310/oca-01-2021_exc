package ueb_weller;

import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class PlzSucheView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//Attribute
	//JLabel
	JLabel titel;
	JLabel lblPlz;
	JLabel lblOrt;
	JLabel lblLand;
	//Textfelder
	JTextField txfPlz;
	JTextField txfOrt;
	JTextField txfLand;
	//Buttons
	JButton btnSuchen;
	JButton btnLoeschen;
	JButton btnBeenden;
	//JPanels
	JPanel pnlTitel;
	JPanel pnlInhalt;
	JPanel pnlButtons;
	JPanel pnlGesamt;
	
	// Controller als Member-Variable
	PlzSucheController controller = new PlzSucheController(this);	
	
	//Constructor
	public PlzSucheView() {
		super("Postleitzahlensucher");	
		initComponents();
		//Titel zum Titel-Panel hinzuf�gen
		pnlTitel.add(titel);
		//Label und Textfelder zum Inhalt-Panel hinzuf�gen
		pnlInhalt.add(lblPlz);
		pnlInhalt.add(txfPlz);
		pnlInhalt.add(lblOrt);
		pnlInhalt.add(txfOrt);
		pnlInhalt.add(lblLand);
		pnlInhalt.add(txfLand);
		// Buttons zum Button-Panel hinzuf�gen
		pnlButtons.add(btnSuchen);
		pnlButtons.add(btnLoeschen);
		pnlButtons.add(btnBeenden);
		// Alle Panels zum GesamtPanel hinzuf�gen
		pnlGesamt.add(pnlTitel);
		pnlGesamt.add(pnlInhalt);
		pnlGesamt.add(pnlButtons);
		// Layout f�r das Panel setzen
		pnlInhalt.setLayout(new GridLayout(3,2,30,30));
		pnlButtons.setLayout(new GridLayout(1,3,20,20));
		pnlGesamt.setLayout(new FlowLayout(FlowLayout.CENTER,2000,30));
		// Setzen des Panels als Inhaltsbereich 
		this.setContentPane(pnlGesamt);
		this.setSize(350, 330); // Gr��e des Fensters in Breite mal H�he
		this.setLocationRelativeTo(null); // Fenster wird mittig angeordnet
		this.setDefaultCloseOperation(EXIT_ON_CLOSE); // Anwendung wird beim Schlie�en des Fensters beendet (EXIT_ON_CLOSE) (DO_NOTHING_ON_CLOSE)
		//WindowListener f�r den JFrame
		this.addWindowListener(controller);
		//ActionListener f�r die Buttons
		btnSuchen.addActionListener(controller);
		btnLoeschen.addActionListener(controller);
		btnBeenden.addActionListener(controller);
		//KeyListener f�r die Textfelder
		txfPlz.addKeyListener(controller);
		txfOrt.addKeyListener(controller);
		txfLand.addKeyListener(controller);
		
		
		
	}
	
	// Initialiseren der Komponenten
	private void initComponents() {
		
		titel = new JLabel("Postleitzahlensuche");
		
		lblPlz = new JLabel("Postleitzahl");
		lblOrt = new JLabel("Ort");
		lblLand = new JLabel("Bundesland");
		
		txfPlz = new JTextField();
		txfOrt = new JTextField();
		txfLand = new JTextField();
		
		btnSuchen = new JButton("Suchen");
		btnLoeschen = new JButton("L�schen");
		btnBeenden = new JButton("Beenden");
		
		pnlTitel = new JPanel();
		pnlInhalt = new JPanel();
		pnlButtons = new JPanel();
		pnlGesamt = new JPanel();
	}
	
}
