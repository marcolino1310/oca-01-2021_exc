package ueb_weller;

public class ZinsrechnerModel implements InterZinsrechnerModel {

	@Override
	public double berechneEndkapital(double startkapital, double zinssatz, double laufzeit) {
		return startkapital * Math.pow(1 + zinssatz / 100, laufzeit);
	}

	@Override
	public double berechneLaufzeit(double startkapital, double zinssatz, double endkapital) {
		double erg = (Math.log(endkapital/startkapital) / Math.log(1 + zinssatz / 100));
		return erg;
	}

}
