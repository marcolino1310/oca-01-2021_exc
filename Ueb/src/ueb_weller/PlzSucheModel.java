package ueb_weller;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class PlzSucheModel implements PLZModel{

	private ArrayList<String> zeilen = new ArrayList<>();
	private String plzDatei = "files/plz.txt";
	
	//Constructor
	public PlzSucheModel() {
		init();
	}
	
	@Override
	public String getOrt(String plz) {
		return getZeile(plz, 1);
	}

	@Override
	public String getLand(String plz) {
		return getZeile(plz,5);
	}
	
	private String getZeile(String plz, int pos) {
		boolean flag = false;
		String ret = null;
		for (String zeile : zeilen) {
			if(zeile.startsWith(plz)) {
				if(flag) {
				ret += ", " + zeile.split("\\t")[pos];
				} else {
					flag = true;
					ret = zeile.split("\\t")[pos];
				}
			}			
		}
		return ret;
	}

	private void init() {
		//try-with-rescources
		try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(plzDatei), "UTF8"))) {
			String zeile;
			while((zeile = br.readLine()) != null) {
				zeilen.add(zeile);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	private void initPre7() {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(plzDatei), "UTF8"));
			String zeile;
			while((zeile = br.readLine()) != null) {
				zeilen.add(zeile);
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
}
