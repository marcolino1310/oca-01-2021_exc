package ueb_weller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.NumberFormat;
import javax.swing.JOptionPane;

public class ZinsrechnerController extends WindowAdapter implements ActionListener, KeyListener{
	
	ZinsrechnerView view;
	ZinsrechnerModel model;
	
	public ZinsrechnerController(ZinsrechnerView view) {
		this.view = view;
		this.model = new ZinsrechnerModel();
	}
	
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == view.btnBeenden) {
			beenden();
		} else if(e.getSource()== view.btnBerechnen) {
			berechnen();
		} else if(e.getSource() == view.rbtEndkapital) {
			loeschen();
			view.txfLaufzeit.setEditable(true);
			view.txfEndkapital.setEditable(false);
		} else if(e.getSource() == view.rbtLaufzeit) {
			loeschen();
			view.txfLaufzeit.setEditable(false);
			view.txfEndkapital.setEditable(true);
			view.rbtLaufzeit.setSelected(true);
		} else if(e.getSource() == view.btnLoeschen) {
			loeschen();
		} else if(e.getSource() == view.menBerechnen) {
			berechnen();
		} else if(e.getSource() == view.menLoeschen) {
			loeschen();
		} else if(e.getSource() == view.menBeenden) {
			beenden();
		} else if(e.getSource() == view.menInfo) {
			info();
		}
	}
	
	private void berechnen() {
		//Inhalt der drei Textfelder auslesen (getText)
		//Umwandeln in die richtigen Datentypen
		//Aufruf der berechneEndkaptial-Methode aus dem Model
		//Ergebnis in Textfeld Enkapital eintragen (setText)
		double startkapital = Double.parseDouble(view.txfStartkapital.getText());
		double zinssatz = Double.parseDouble(view.txfZinssatz.getText());
		String laufzeitString = null;
		String endkapitalString = null;
		double laufzeit = 0.0;
		double endkapital = 0.0;		
		
		if(view.rbtEndkapital.isSelected()) {
			laufzeit = Double.parseDouble(view.txfLaufzeit.getText());
			endkapital = model.berechneEndkapital(startkapital, zinssatz, laufzeit);
			//String endkapitalString = String.format("%,.2f �", endkapital);
			endkapitalString = NumberFormat.getCurrencyInstance().format(endkapital);
			view.txfEndkapital.setText(endkapitalString);			// Ausgabe in das Endkapital-Textfeld			
		}
		else if (view.rbtLaufzeit.isSelected()){
			endkapital = Double.parseDouble(view.txfEndkapital.getText());
			laufzeit = model.berechneLaufzeit(startkapital, zinssatz, endkapital);
			NumberFormat nf = NumberFormat.getInstance();
			nf.setMaximumFractionDigits(1);		//Anzahl der maximalen Nachkommastellen
			laufzeitString = nf.format(laufzeit);			
			view.txfLaufzeit.setText(laufzeitString);
		}
		view.txfArea.setText("");
//		double erg = startkapital;
//		String ergString;
//		for (int i = 0; i < laufzeit; i++) {
//			erg = erg + ((erg/100)*zinssatz);
//			ergString = Double.toString(erg);
//			ergString = NumberFormat.getCurrencyInstance().format(erg);
//			view.txfArea.append((i+1) + "          " + ergString + "\n");
//		}
		
		StringBuilder sbVerlauf = new StringBuilder();
		for (int jahr = 1; jahr <= laufzeit; jahr++) {
			sbVerlauf.append(String.format("%2d\t%,15.2f �%n", jahr, model.berechneEndkapital(startkapital, zinssatz, jahr)));
		}
		view.txfArea.setText(sbVerlauf.toString());
		
	}

	
	/**
	 * Inhalt der vier Textfelder l�schen
	 */
	private void loeschen() {
		view.txfStartkapital.setText("");
		view.txfZinssatz.setText("");
		view.txfLaufzeit.setText("");
		view.txfEndkapital.setText("");
		view.txfArea.setText("");
		view.txfStartkapital.requestFocus(); //Setzt den Curser in das Feld Startkapital
	}
	
	
	
	private void beenden() {
		int antwort = JOptionPane.showConfirmDialog(view, "Sind Sie sicher ?", "Beenden", JOptionPane.YES_NO_OPTION);
		if (antwort == JOptionPane.YES_NO_OPTION) {
			System.exit(0);
		}
	}
	
	private void info() {
		JOptionPane.showMessageDialog(view, "Zinsrechner v.1.01 \n  copyright 2021 Sebastian Weller", "Info", JOptionPane.INFORMATION_MESSAGE);		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		beenden();
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
//		if(arg0.getKeyChar() == KeyEvent.VK_ENTER)
//		System.out.println("Enter gedr�ckt" + arg0);
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
//		if(arg0.getKeyChar() == KeyEvent.VK_ENTER)
//			System.out.println("Enter gedr�ckt" + arg0);
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		if(arg0.getKeyChar() == KeyEvent.VK_ENTER) {
			if(arg0.getSource() == view.txfStartkapital) {
				view.txfZinssatz.requestFocus();
			}else if(arg0.getSource() == view.txfZinssatz) {
				if(view.rbtEndkapital.isSelected()) {
					view.txfLaufzeit.requestFocus();
				}else if(view.rbtLaufzeit.isSelected()) {
						view.txfEndkapital.requestFocus();
					}
				}else if (arg0.getSource() == view.txfLaufzeit || arg0.getSource() == view.txfEndkapital) {
					berechnen();
				}
		}
	}

}
