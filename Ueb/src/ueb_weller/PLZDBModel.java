package ueb_weller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class PLZDBModel implements PLZModel {

	private String accessConnect = "jdbc:ucanaccess://files/PLZ_Datenbank.accdb";
	private Connection verbindung;
	private Statement befehl;
	
	public PLZDBModel() {
		//Treiber laden
//		try {
//			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver"); //Treiberklasse f�r Access laden
//			System.out.println("Ucanacces Treiber Laden erfolgreich");
//		} catch (ClassNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		try {
			verbindung = DriverManager.getConnection(accessConnect);
			System.out.println("Verbindung erfolgreich zur DB aufgebaut");
			befehl = verbindung.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	@Override
	public String getOrt(String plz) {
		// select Ort from tblPLZ where PLZ = 12459    <- SQL Befehl
		boolean flag = false;
		String ret = null;		
		try {
			ResultSet rs = befehl.executeQuery("select Ort from tblPLZ where PLZ = " + plz);
			while(rs.next()) {
				if(flag) {
					ret += ", " + rs.getString("Ort");
				}else {
					flag = true;
					ret = rs.getString("Ort");
				}
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ret;
	}

	@Override
	public String getLand(String plz) {
		// select Bundesland from tblPLZ where PLZ = 12459    <- SQL Befehl
				boolean flag = false;
				String ret = null;		
				try {
					ResultSet rs = befehl.executeQuery("select Bundesland from tblPLZ where PLZ = " + plz);
					while(rs.next()) {
						if(flag) {
							ret += ", " + rs.getString("Bundesland");
						}else {
							flag = true;
							ret = rs.getString("Bundesland");
						}
						
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return ret;
	}

}
