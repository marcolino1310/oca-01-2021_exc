package ueb_weller;

public interface InterZinsrechnerModel {
	
	double berechneEndkapital(double startkapital, double zinssatz, double laufzeit);

	double berechneLaufzeit(double startkapital, double zinssatz, double endkapital);
	
}
