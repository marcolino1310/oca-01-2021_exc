package ueb_weller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class PlzSucheController extends WindowAdapter implements ActionListener, KeyListener{
	
	PlzSucheView view;
	//PlzSucheModel model;
	PLZModel model;
	
	public PlzSucheController(PlzSucheView view) {
		this.view = view;
		//this.model = new PlzSucheModel();
		this.model =  PLZModel.getInstance(PLZModel.Modus.DB);
	}
	
	/**
	 * beenden beendet das Programm
	 */
	public void beenden() {
		System.exit(0);
	}

	
	/**
	 * loeschen - löscht alle Textfelder. Dabei werden sie mit Leerzeichen überschrieben.
	 */
	public void loeschen() {
		view.txfPlz.setText("");
		view.txfOrt.setText("");
		view.txfLand.setText("");
	}
	
	
//	public void suchen() {
//		if(!view.txfPlz.getText().isEmpty()) { 
//			//aus der Plz-Datei lesen
//			BufferedReader bw = null;
//		
//			try {
//				bw = new BufferedReader(new FileReader(new File("files/plz.txt")));
//				String line = null;
//				while (( line = bw.readLine()) != null) {
//					String plz = line.substring(0,5);
//					if(plz.equalsIgnoreCase(view.txfPlz.getText())) {
//						StringBuilder sbline = new StringBuilder(line.replace("\t", ";")); //Ersetzt die Tabulator-Zeichen in der Textdatei durch Semikolons
//						String sline = sbline.toString();												
//						char[] cArr = sline.toCharArray();
//						char[] posArr = {0,0,0,0,0};
//						int semcounter = 0;
//						for (int i = 0; i < cArr.length; i++) {
//							if(cArr[i] == ';') {
//								posArr[semcounter] = (char)i;
//								semcounter++;								
//							}
//						}						
//						String ort = line.substring((int)posArr[0], (int) posArr[1]);
//						String land = line.substring(posArr[4]);
//						view.txfOrt.setText(ort);
//						//view.txfOrt.setText(model.getOrt(plz));
//						view.txfLand.setText(land);	
//						//view.txfLand.setText(model.getLand(plz));
//						
//						bw.close();
//						return;
//					}
//				}
//			} catch (FileNotFoundException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} 
//			
//		}
//		if(!view.txfOrt.getText().isEmpty()) System.out.println(view.txfOrt.getText());
//		if(!view.txfLand.getText().isEmpty()) System.out.println(view.txfLand.getText());
//	}
	
	
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getSource() == view.btnSuchen) {
			String plz = view.txfPlz.getText(); //"86733"; //47058 03172
			view.txfOrt.setText(model.getOrt(plz)); //System.out.println("Ort: " + ort);
			view.txfLand.setText(model.getLand(plz));
			//suchen();
		}
		if(arg0.getSource() == view.btnLoeschen) {
			loeschen();
		}
		if(arg0.getSource() == view.btnBeenden) {
			beenden();
		}
		
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
}
