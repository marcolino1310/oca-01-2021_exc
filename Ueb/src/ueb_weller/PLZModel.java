package ueb_weller;

public interface PLZModel {

	enum Modus{FILE,DB}
	
	String getOrt(String plz); // imnplizit abstract
	
	String getLand(String plz);
	
	static PLZModel getInstance(Modus modus) {
		if(modus == Modus.FILE) {
			return new PLZFileModel();
			
		} else if (modus == Modus.DB) {
			return new PLZDBModel();
		}
		return null;
	}
}
