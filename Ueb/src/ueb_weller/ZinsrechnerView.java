package ueb_weller;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;

import javax.swing.*;

public class ZinsrechnerView extends JFrame{

	private static final long serialVersionUID = 1L;
	// Deklaration der Komponenten
	//JMenuBar
	JMenuBar menueBar;
	//JMenu
	JMenu datei;
	JMenu info;
	//JMenu Items
	JMenuItem menBerechnen;
	JMenuItem menLoeschen;
	JMenuItem menBeenden;
	JMenuItem menInfo;
	// Bezeichnungsfelder (Label)
	JLabel lblTitel;
	JLabel lblStartkapital;
	JLabel lblZinssatz;
	JLabel lblLaufzeit;
	JLabel lblEndkapital;
	// Textfelder
	JTextField txfStartkapital;
	JTextField txfZinssatz;
	JTextField txfLaufzeit;
	JTextField txfEndkapital;
	// TextArea
	JTextArea txfArea;	
	// Befehls-Schatlf�chen
	JButton btnBerechnen;
	JButton btnLoeschen;
	JButton btnBeenden;
	//OptionsFelder (RadioButton)
	JRadioButton rbtEndkapital;
	JRadioButton rbtLaufzeit;
	//ButtonGroup zum Gruppieren der Optionsfelder
	ButtonGroup btgModus;
	// Container f�r die Komponenten (JPanel)
	JPanel pnlInhalt;
	JPanel pnlTitel;
	JPanel pnlAuswahlListe;
	JPanel pnlAnzeige;
	JPanel pnlGesamt;
	//Scroll-Pane
	JScrollPane scpVerlauf;
	// Controller als Member-Variable
	ZinsrechnerController controller = new ZinsrechnerController(this);
	
	public ZinsrechnerView() {
		super("Zinsrechner"); // Titelleiste
//		this.setTitle("Zinsrechner"); // Alternative
		initComponents(); // Initialisierung der Componenten
//		Hinzuf�gen der Komponenten zum Container
		pnlTitel.add(lblTitel);
		pnlInhalt.add(lblStartkapital);
		pnlInhalt.add(txfStartkapital);
		pnlInhalt.add(lblZinssatz);
		pnlInhalt.add(txfZinssatz);
		pnlInhalt.add(lblLaufzeit);
		pnlInhalt.add(txfLaufzeit);
		pnlInhalt.add(lblEndkapital);
		pnlInhalt.add(txfEndkapital);
		pnlInhalt.add(rbtEndkapital);
		btgModus.add(rbtEndkapital);
		btgModus.add(rbtLaufzeit);
		pnlInhalt.add(rbtLaufzeit);
		pnlAuswahlListe.add(btnBerechnen);
		pnlAuswahlListe.add(btnLoeschen);
		pnlAuswahlListe.add(btnBeenden);
		pnlAnzeige.add(scpVerlauf);
		

		
		pnlGesamt.add(pnlTitel);
		pnlGesamt.add(pnlInhalt);
		pnlGesamt.add(pnlAuswahlListe);
		pnlGesamt.add(pnlAnzeige);
		pnlGesamt.add(menueBar);
		
		
//		ActionListener f�r den Button Beenden ohne Model-View-Controller
//		btnBeenden.addActionListener(e -> System.exit(ABORT));
//		btnBeenden.addActionListener(new ActionListener() {
//			
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				System.exit(0);
//				
//			}
//		});
		//WindowListener f�r den JFRame
		this.addWindowListener(controller);
//		ActionListener f�r die Button und den Button Beenden mit Model-View-Controller
		btnBerechnen.addActionListener(controller);
		btnBeenden.addActionListener(controller);
		btnLoeschen.addActionListener(controller);
		rbtEndkapital.addActionListener(controller);
		rbtLaufzeit.addActionListener(controller);
		menBerechnen.addActionListener(controller);
		menLoeschen.addActionListener(controller);
		menBeenden.addActionListener(controller);
		menInfo.addActionListener(controller);
		//KeyListener
		txfStartkapital.addKeyListener(controller);
		txfZinssatz.addKeyListener(controller);
		txfLaufzeit.addKeyListener(controller);
		txfEndkapital.addKeyListener(controller);

		
//		Layout des Panels setzen
		pnlInhalt.setLayout(new GridLayout(5, 2, 30, 30));
		pnlAuswahlListe.setLayout(new GridLayout(1,3,10,30));
		pnlGesamt.setLayout(new FlowLayout(FlowLayout.CENTER, 2000, 30));
		rbtEndkapital.setSelected(true);	//Optionsfeld beim Start aktiviert
		txfEndkapital.setEditable(false); 	//Textfeld Endkapital nicht editierbar
		txfArea.setFont(new Font("Courier New", Font.PLAIN, 12));
		scpVerlauf.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		pnlGesamt.setBackground(Color.ORANGE);	//Hintergrundfarbe des Inhaltsbereichs
		pnlTitel.setOpaque(false);			//setzt den Hintergrund durchsichtig
		pnlAnzeige.setOpaque(false);
		pnlAuswahlListe.setOpaque(false);
		pnlInhalt.setOpaque(false);
		rbtEndkapital.setOpaque(false);
		rbtLaufzeit.setOpaque(false);
		
		//Menu� bauen
		menueBar.add(datei);
		datei.setMnemonic('D');
		menueBar.add(info);
		info.setMnemonic('i');
		datei.add(menBerechnen);
		datei.add(menLoeschen);
		datei.add(menBeenden);
		info.add(menInfo);
		this.setJMenuBar(menueBar);
		menBerechnen.setMnemonic(KeyEvent.VK_B);
		menLoeschen.setMnemonic(KeyEvent.VK_L);
		menBeenden.setMnemonic(KeyEvent.VK_E);
		menInfo.setMnemonic(KeyEvent.VK_I);
		menInfo.setAccelerator(KeyStroke.getKeyStroke("control I"));
		
//		Setzen des Panels als Inhaltsbereich 
		this.setContentPane(pnlGesamt);
		this.setSize(350, 650); // Gr��e des Fensters in Breite mal H�he
		this.setLocationRelativeTo(null); // Fenster wird mittig angeordnet
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE); // Anwendung wird beim Schlie�en des Fensters beendet (EXIT_ON_CLOSE)
		this.setVisible(true); // Fesnster wird sichtbar gemacht
	}
	
	private void initComponents() {
		lblTitel = new JLabel("Zinsrechner");
		lblStartkapital = new JLabel("Startkapital");
		lblZinssatz = new JLabel("Zinssatz");
		lblLaufzeit = new JLabel("Laufzeit");
		lblEndkapital = new JLabel("Endkapital");
		
		txfStartkapital = new JTextField(10);
		txfZinssatz = new JTextField(10);
		txfLaufzeit = new JTextField(10);
		txfEndkapital = new JTextField(10);
		
		txfArea = new JTextArea(10,30);
		scpVerlauf = new JScrollPane(txfArea);
		
		rbtEndkapital = new JRadioButton("Endkapital");
		rbtLaufzeit = new JRadioButton("Laufzeit");
		btgModus= new ButtonGroup();
			
		btnBerechnen = new JButton("Berechnen");
		btnLoeschen = new JButton("L�schen");
		btnBeenden = new JButton("Beenden");
		
		pnlInhalt = new JPanel();
		pnlTitel = new JPanel();
		pnlAuswahlListe = new JPanel();
		pnlAnzeige = new JPanel();
		pnlGesamt = new JPanel();
		
		//JMenu Bar
		menueBar = new JMenuBar();
		datei = new JMenu("Datei");
		info = new JMenu("Info");
		menBerechnen = new JMenuItem("Berechnen");
		menLoeschen = new JMenuItem("L�schen");
		menBeenden = new JMenuItem("Beenden");
		menInfo = new JMenuItem("�ber Zinsrechner");
	}

}
