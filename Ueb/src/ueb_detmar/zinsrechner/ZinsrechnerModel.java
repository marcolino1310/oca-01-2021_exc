package ueb_detmar.zinsrechner;

public class ZinsrechnerModel implements InterZinsrechnerModel {

	@Override
	public double berechneEndkapital(int startkapital, double zinssatz, int laufzeit) {
		return startkapital * Math.pow(1 + zinssatz / 100, laufzeit);
	}

	@Override
	public double berechneLaufzeit(int startkapital, double zinssatz, int endkapital) {
		return Math.log(endkapital * 1.0 / startkapital) / Math.log1p(zinssatz / 100);
	}
}
