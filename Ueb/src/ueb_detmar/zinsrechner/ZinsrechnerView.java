package ueb_detmar.zinsrechner;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

public class ZinsrechnerView extends JFrame {
	private static final long serialVersionUID = -1395197503307301220L;

	JLabel lblStartkapital;
	JLabel lblZinssatz;
	JLabel lblLaufzeit;
	JLabel lblEndkapital;
	JLabel lblTitel;

	JTextField txfStartkapital;
	JTextField txfZinssatz;
	JTextField txfLaufzeit;
	JTextField txfEndkapital;

	JButton btnBerechnen;
	JButton btnBeenden;
	JButton btnLoeschen;

	JRadioButton rbtEndkapital;
	JRadioButton rbtLaufzeit;
	ButtonGroup btgModus;

	JTextArea txaVerlauf;
	JScrollPane scpVerlauf;

	JMenuBar mbMenu;
	JMenu mnuDatei;
	JMenu mnuInfo;
	JMenuItem mniBerechnen;
	JMenuItem mniLoeschen;
	JMenuItem mniBeenden;
	JMenuItem mniInfo;

	JPanel pnlInhalt;
	JPanel pnlTitel;
	JPanel pnlButton;
	JPanel pnlTextArea;
	JPanel pnlGesamt;

	ZinsrechnerController controller = new ZinsrechnerController(this);

	Font font;

	public ZinsrechnerView() {
		super("Zinsrechner");
		intitComponents();
		pnlTitel.add(lblTitel);
		pnlInhalt.add(lblStartkapital);
		pnlInhalt.add(txfStartkapital);
		pnlInhalt.add(lblZinssatz);
		pnlInhalt.add(txfZinssatz);
		pnlInhalt.add(lblLaufzeit);
		pnlInhalt.add(txfLaufzeit);
		pnlInhalt.add(lblEndkapital);
		pnlInhalt.add(txfEndkapital);
		pnlInhalt.add(rbtEndkapital);
		pnlInhalt.add(rbtLaufzeit);

		btgModus.add(rbtEndkapital);
		btgModus.add(rbtLaufzeit);

		pnlButton.add(btnBerechnen);
		pnlButton.add(btnLoeschen);
		pnlButton.add(btnBeenden);

		pnlTextArea.add(scpVerlauf);

		pnlGesamt.add(pnlTitel);
		pnlGesamt.add(pnlInhalt);
		pnlGesamt.add(pnlButton);
		pnlGesamt.add(pnlTextArea);

		btnBerechnen.addActionListener(controller);
		btnBeenden.addActionListener(controller);
		btnLoeschen.addActionListener(controller);
		rbtEndkapital.addActionListener(controller);
		rbtLaufzeit.addActionListener(controller);

		txfStartkapital.addKeyListener(controller);
		txfZinssatz.addKeyListener(controller);
		txfLaufzeit.addKeyListener(controller);
		txfEndkapital.addKeyListener(controller);

		pnlInhalt.setLayout(new GridLayout(5, 2, 30, 30));
		pnlGesamt.setLayout(new FlowLayout(FlowLayout.CENTER, 1000, 30));

		mnuDatei.add(mniBerechnen);
		mnuDatei.add(mniLoeschen);
		mnuDatei.add(mniBeenden);
		mnuInfo.add(mniInfo);
		mbMenu.add(mnuDatei);
		mbMenu.add(mnuInfo);
		setJMenuBar(mbMenu);

		mniBerechnen.addActionListener(controller);
		mniLoeschen.addActionListener(controller);
		mniBeenden.addActionListener(controller);
		mniInfo.addActionListener(controller);

		mnuDatei.setMnemonic(KeyEvent.VK_D);
		mnuInfo.setMnemonic(KeyEvent.VK_I);
		mniBerechnen.setMnemonic(KeyEvent.VK_B);
		mniLoeschen.setMnemonic(KeyEvent.VK_L);
		mniBeenden.setMnemonic(KeyEvent.VK_E);
		mniInfo.setMnemonic(KeyEvent.VK_I);
		
		mniLoeschen.setAccelerator(KeyStroke.getKeyStroke("control L"));
		mniBeenden.setAccelerator(KeyStroke.getKeyStroke("alt F4"));
		mniInfo.setAccelerator(KeyStroke.getKeyStroke("control I"));

		txfStartkapital.setFont(font);
		txfZinssatz.setFont(font);
		txfLaufzeit.setFont(font);
		txfEndkapital.setFont(font);
		txaVerlauf.setFont(font);

		addWindowListener(controller);

		setContentPane(pnlGesamt);
		setSize(350, 650);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setVisible(true);
	}

	private void intitComponents() {
		int width = 10;
		lblStartkapital = new JLabel("Startkapital");
		lblZinssatz = new JLabel("Zinssatz");
		lblLaufzeit = new JLabel("Laufzeit");
		lblEndkapital = new JLabel("Endkapital");
		lblTitel = new JLabel("Zinsrechner");

		txfStartkapital = new JTextField(width);
		txfZinssatz = new JTextField(width);
		txfLaufzeit = new JTextField(width);
		txfEndkapital = new JTextField(width);
		txfEndkapital.setEditable(false);
		txfEndkapital.setFocusable(false);

		btnBerechnen = new JButton("Berechnen");
		btnLoeschen = new JButton("L�schen");
		btnBeenden = new JButton("Beenden");

		btgModus = new ButtonGroup();

		rbtEndkapital = new JRadioButton("Endkapital", true);
		rbtLaufzeit = new JRadioButton("Laufzeit");

		txaVerlauf = new JTextArea(10, 25);
		txaVerlauf.setEditable(false);
		txaVerlauf.setFocusable(false);
		scpVerlauf = new JScrollPane(txaVerlauf, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		pnlInhalt = new JPanel();
		pnlTitel = new JPanel();
		pnlButton = new JPanel();
		pnlTextArea = new JPanel();
		pnlGesamt = new JPanel();

		font = new Font("Courier New", Font.PLAIN, 12);

		mbMenu = new JMenuBar();
		mnuDatei = new JMenu("Datei");
		mnuInfo = new JMenu("Info");
		mniBerechnen = new JMenuItem("Berechnen");
		mniLoeschen = new JMenuItem("L�schen");
		mniBeenden = new JMenuItem("Beenden");
		mniInfo = new JMenuItem("Info", KeyEvent.VK_I);
	}
}
