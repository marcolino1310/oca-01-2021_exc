package ueb_detmar.zinsrechner;

import java.awt.EventQueue;

public class ZinsrechnerTest {
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				@SuppressWarnings("unused")
				ZinsrechnerView view = new ZinsrechnerView();
			}
		});
	}
}
