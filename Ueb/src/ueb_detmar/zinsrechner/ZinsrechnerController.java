package ueb_detmar.zinsrechner;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class ZinsrechnerController extends WindowAdapter implements ActionListener, KeyListener {
	ZinsrechnerView view;
	ZinsrechnerModel model;

	public ZinsrechnerController(ZinsrechnerView view) {
		this.view = view;
		this.model = new ZinsrechnerModel();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == view.btnBeenden || e.getSource() == view.mniBeenden) {
			beenden();
		} else if (e.getSource() == view.btnBerechnen || e.getSource() == view.mniBerechnen) {
			berechnen();
		} else if (e.getSource() == view.btnLoeschen || e.getSource() == view.mniLoeschen) {
			loeschen();
		} else if (e.getSource() == view.rbtEndkapital) {
			changeActiveTxf(view.txfEndkapital, view.txfLaufzeit);
		} else if (e.getSource() == view.rbtLaufzeit) {
			changeActiveTxf(view.txfLaufzeit, view.txfEndkapital);
		} else if (e.getSource() == view.mniInfo) {
			info();
		}
	}

	private void changeActiveTxf(JTextField active, JTextField inactive) {
		active.setEditable(false);
		inactive.setEditable(true);
		active.setFocusable(false);
		inactive.setFocusable(true);
		view.txfEndkapital.setText("");
		view.txfLaufzeit.setText("");
		view.txaVerlauf.setText("");
	}

	private void info() {
		JOptionPane.showMessageDialog(view, "Zinsrechner" + System.lineSeparator() + "Version von heute", "Info",
				JOptionPane.INFORMATION_MESSAGE);
	}

	private void beenden() {
		if (JOptionPane.showConfirmDialog(view, "wirklich beenden?", "Beenden", JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE) == 0)
			System.exit(0);
	}

	private void loeschen() {
		view.txfStartkapital.setText("");
		view.txfZinssatz.setText("");
		view.txfLaufzeit.setText("");
		view.txfEndkapital.setText("");
		view.txfStartkapital.requestFocus();
		view.txaVerlauf.setText("");
	}

	private double readAndCheckInput(JTextField txfInput) {
		if (txfInput.getText().trim().equals("")) {
			JOptionPane.showMessageDialog(view, "Bitte alle Felder f�llen!", "Fehler", JOptionPane.ERROR_MESSAGE);
			txfInput.requestFocus();
			txfInput.selectAll();
			return -1;
		}
		try {
			double d = Double.parseDouble(txfInput.getText().replace(',', '.'));
			if (d <= 0) {
				JOptionPane.showMessageDialog(view, "Bitte eine Zahl gr��er 0 eingeben!", "Fehler",
						JOptionPane.ERROR_MESSAGE);
				txfInput.requestFocus();
				txfInput.selectAll();
				d = -1;
			}
			return d;
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(view, "Ung�ltige Eingabe" + System.lineSeparator() + "Nur Zahlen erlaubt!",
					"Fehler", JOptionPane.ERROR_MESSAGE);
		}
		txfInput.requestFocus();
		txfInput.selectAll();
		return -1;
	}

	private void berechnen() {
		double startkapital;
		double zinssatz;
		double laufzeit;
		double endkapital;
		if ((startkapital = readAndCheckInput(view.txfStartkapital)) < 0) {
			return;
		}
		if ((zinssatz = readAndCheckInput(view.txfZinssatz)) < 0) {
			return;
		}

		if (view.rbtEndkapital.isSelected()) {
			if ((laufzeit = readAndCheckInput(view.txfLaufzeit)) < 0) {
				return;
			}
			endkapital = model.berechneEndkapital((int) startkapital, zinssatz, (int) laufzeit);
			view.txfEndkapital.setText(String.format("%.2f", endkapital));
		} else {
			if ((endkapital = readAndCheckInput(view.txfEndkapital)) < 0) {
				return;
			}
			laufzeit = model.berechneLaufzeit((int) startkapital, zinssatz, (int) endkapital);
			String ls = String.format("%.1f", Math.abs(laufzeit));
			if (laufzeit > 0) {
				view.txfLaufzeit.setText(ls);
			} else {
				JOptionPane.showMessageDialog(view,
						"H�tten Sie " + (int) endkapital + " � vor " + ls + " Jahren angelegt" + System.lineSeparator()
								+ "H�tten Sie jetzt " + (int) startkapital + " �",
						"Info", JOptionPane.INFORMATION_MESSAGE);
			}
		}

		view.txaVerlauf.setText("");
		for (int i = 1; i <= laufzeit; i++) {
			view.txaVerlauf.append(String.format("%2d %,15.2f �",
					new Object[] { i, model.berechneEndkapital((int) startkapital, zinssatz, i) }));
			if (i != (int) laufzeit)
				view.txaVerlauf.append(System.lineSeparator());
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		if (e.getKeyChar() == KeyEvent.VK_ENTER) {
			if (view.txfStartkapital.hasFocus()) {
				view.txfZinssatz.requestFocus();
				view.txfZinssatz.selectAll();
			} else if (view.txfZinssatz.hasFocus()) {
				if (view.rbtLaufzeit.isSelected()) {
					view.txfEndkapital.requestFocus();
					view.txfEndkapital.selectAll();
				} else {
					view.txfLaufzeit.requestFocus();
					view.txfLaufzeit.selectAll();
				}
			} else if (view.txfLaufzeit.hasFocus() || view.txfEndkapital.hasFocus()) {
				berechnen();
			}
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void windowClosing(WindowEvent e) {
		beenden();
	}
}
