package ueb_detmar.pizza;

import java.util.ArrayList;
import java.util.Arrays;

public class Pizza {
	public static void main(String[] args) throws PizzaException {
		ArrayList<String> belag = new ArrayList<>(
				Arrays.asList("Tomate", "Pilze", "Salami", "Schinken", "Artischocken", "K�se", "Broccoli"));
		if (args.length > 3)
			throw new PizzaException("zu viele Bel�ge gew�hlt");
		for (int i = 0; i < args.length; i++) {
			if (!belag.contains(args[i]))
				throw new PizzaException("falscher Belag: " + args[i]);
		}
		for (int i = 0; i < args.length; i++)
			System.out.println(args[i]);
	}
}
