package ueb_detmar.pizza;

class PizzaException extends Exception {
	public PizzaException() {

	}

	public PizzaException(String message) {
		super(message);
	}
}