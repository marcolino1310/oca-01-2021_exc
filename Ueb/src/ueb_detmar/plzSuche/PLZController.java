package ueb_detmar.plzSuche;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JOptionPane;

import ueb_detmar.plzSuche.PLZModel.Modus;

public class PLZController extends KeyAdapter implements ActionListener {
	PLZView view;
	PLZModel model;

	public PLZController(PLZView view) {
		this.view = view;
		this.model = PLZModel.getInstance(Modus.FILE);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == view.btnSuchen || e.getSource() == view.mniSuchen) {
			suchen();
		} else if (e.getSource() == view.btnLoeschen || e.getSource() == view.mniLoeschen) {
			loeschen();
		} else if (e.getSource() == view.btnBeenden || e.getSource() == view.mniBeenden) {
			beenden();
		} else if (e.getSource() == view.mniInfo) {
			info();
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		if (e.getKeyChar() == KeyEvent.VK_ENTER) {
			suchen();
		}
	}

	private void suchen() {
		String plz = view.txfPlz.getText().trim();

		if (plz.length() > 5) {
			falscheEingabe("darf nur 5 Stellen betragen");
			return;
		}
		if (plz.equals("")) {
			falscheEingabe("Bitte eine PLZ eingeben");
			return;
		}
		try {
			Integer.parseInt(plz);
		} catch (NumberFormatException e) {
			falscheEingabe("darf nur Ziffern beinhalten");
		}
		while (plz.length() < 5) {
			plz = 0 + plz;
		}

		if (model.setPlz(plz)) {
			view.txfOrt.setText(model.getOrt());
			view.txfBland.setText(model.getbLand());
		} else {
			JOptionPane.showMessageDialog(view, "Postleitzahl nicht gefunden", "Fehler", JOptionPane.ERROR_MESSAGE);
		}
	}

	private void falscheEingabe(String fehler) {
		JOptionPane.showMessageDialog(view, "Postleitzahl ung�ltig" + System.lineSeparator() + fehler, "Fehler",
				JOptionPane.ERROR_MESSAGE);
		view.txfPlz.requestFocus();
		view.txfPlz.selectAll();
	}

	private void loeschen() {
		view.txfPlz.setText("");
		view.txfOrt.setText("");
		view.txfBland.setText("");
	}

	private void beenden() {
		System.exit(0);
	}

	private void info() {
		JOptionPane.showMessageDialog(view, "Postleitzahlsuche" + System.lineSeparator() + "Version von heute", "Info",
				JOptionPane.INFORMATION_MESSAGE);
	}
}
