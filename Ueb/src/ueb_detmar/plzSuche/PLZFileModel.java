package ueb_detmar.plzSuche;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class PLZFileModel implements PLZModel {
	private String plz;
	private String ort;
	private String bLand;
	private File plzFile = new File("files/plz.txt");;
	private ArrayList<String> lines = new ArrayList<>();

	PLZFileModel() {
		init();
	}

	private void init() {
		try (BufferedReader bf = new BufferedReader(new InputStreamReader(new FileInputStream(plzFile), "UTF8"))) {
			String line;
			while ((line = bf.readLine()) != null) {
				lines.add(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean setPlz(String plz) {
		this.plz = plz;
		ort = "";
		bLand = "";

		for (String line : lines) {
			String[] temp = line.split("\t");
			if (temp[0].equals(plz)) {
				ort += ", " + temp[1];
				bLand += ", " + temp[5];
			}
		}
		if (!ort.equals("") && !bLand.equals("")) {
			ort = ort.substring(2);
			bLand = bLand.substring(2);
			return true;
		}
		return false;
	}

	@Override
	public String getPlz() {
		return plz;
	}

	@Override
	public String getOrt() {
		return ort;
	}

	@Override
	public String getbLand() {
		return bLand;
	}
}
