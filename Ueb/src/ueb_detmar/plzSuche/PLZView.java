package ueb_detmar.plzSuche;

import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

public class PLZView extends JFrame {
	private static final long serialVersionUID = -1672868752808717223L;
	
	JLabel lblPlzSuche;
	JLabel lblPlz;
	JLabel lblOrt;
	JLabel lblBLand;

	JTextField txfPlz;
	JTextField txfOrt;
	JTextField txfBland;

	JButton btnSuchen;
	JButton btnLoeschen;
	JButton btnBeenden;

	JMenuBar menu;
	JMenu mnuDatei;
	JMenu mnuInfo;
	JMenuItem mniSuchen;
	JMenuItem mniLoeschen;
	JMenuItem mniBeenden;
	JMenuItem mniInfo;

	JPanel pnlTitel;
	JPanel pnlSuche;
	JPanel pnlButton;
	JPanel pnlGesamt;
	
	PLZController controller = new PLZController(this);

	public PLZView() {
		super("Postleitzahlsuche");
		initComponents();
		
		pnlTitel.add(lblPlzSuche);
		pnlSuche.add(lblPlz);
		pnlSuche.add(txfPlz);
		pnlSuche.add(lblOrt);
		pnlSuche.add(txfOrt);
		pnlSuche.add(lblBLand);
		pnlSuche.add(txfBland);
		pnlButton.add(btnSuchen);
		pnlButton.add(btnLoeschen);
		pnlButton.add(btnBeenden);
		pnlGesamt.add(pnlTitel);
		pnlGesamt.add(pnlSuche);
		pnlGesamt.add(pnlButton);
		
		btnSuchen.addActionListener(controller);
		btnLoeschen.addActionListener(controller);
		btnBeenden.addActionListener(controller);
		
		txfPlz.addKeyListener(controller);
		
		setJMenuBar(menu);
		menu.add(mnuDatei);
		menu.add(mnuInfo);
		mnuDatei.add(mniSuchen);
		mnuDatei.add(mniLoeschen);
		mnuDatei.add(mniBeenden);
		mnuInfo.add(mniInfo);
		mniSuchen.addActionListener(controller);
		mniLoeschen.addActionListener(controller);
		mniBeenden.addActionListener(controller);
		mniInfo.addActionListener(controller);
		
		mnuDatei.setMnemonic('d');
		mnuInfo.setMnemonic('i');
		mniSuchen.setMnemonic('s');
		mniLoeschen.setMnemonic('l');
		mniBeenden.setMnemonic('e');
		mniInfo.setMnemonic('i');
		
		mniSuchen.setAccelerator(KeyStroke.getKeyStroke("control S"));
		mniLoeschen.setAccelerator(KeyStroke.getKeyStroke("control L"));
		mniBeenden.setAccelerator(KeyStroke.getKeyStroke("alt F4"));
		mniInfo.setAccelerator(KeyStroke.getKeyStroke("control I"));
		
		pnlSuche.setLayout(new GridLayout(3, 2, 10, 10));
		pnlButton.setLayout(new GridLayout(1, 3, 10, 10));
		pnlGesamt.setLayout(new FlowLayout(FlowLayout.CENTER, 1000, 30));
		
		setContentPane(pnlGesamt);
		setSize(350, 350);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}

	private void initComponents() {
		lblPlzSuche = new JLabel("Postleitzahlensuche");
		lblPlz = new JLabel("Postleitzahl");
		lblOrt = new JLabel("Ort");
		lblBLand = new JLabel("Bundesland");

		txfPlz = new JTextField(13);
		txfOrt = new JTextField(13);
		txfBland = new JTextField(13);
		
		txfOrt.setEditable(false);
		txfBland.setEditable(false);
		txfOrt.setFocusable(false);
		txfBland.setFocusable(false);

		btnSuchen = new JButton("Suchen");
		btnLoeschen = new JButton("L�schen");
		btnBeenden = new JButton("Beenden");

		menu = new JMenuBar();
		mnuDatei = new JMenu("Datei");
		mnuInfo = new JMenu("Info");
		mniSuchen = new JMenuItem("Suchen");
		mniLoeschen = new JMenuItem("L�schen");
		mniBeenden = new JMenuItem("Beenden");
		mniInfo = new JMenuItem("Info");
		
		pnlTitel = new JPanel();
		pnlSuche = new JPanel();
		pnlButton = new JPanel();
		pnlGesamt = new JPanel();
	}
}
