package ueb_detmar.plzSuche;

import java.awt.EventQueue;

import ueb_detmar.plzSuche.PLZModel.Modus;

public class PLZTest {
	public static void main(String[] args) {
		PLZModel m = PLZModel.getInstance(Modus.FILE);

		String plz = "03172";
		System.out.println(m.setPlz(plz));
		System.out.println(m.getOrt());
		System.out.println(m.getbLand());

		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				new PLZView();
			}
		});
	}
}
