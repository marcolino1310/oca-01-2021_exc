package ueb_detmar.plzSuche;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class PLZDBModel implements PLZModel {
	private String plz;
	private String ort;
	private String bLand;

	private String accessConnect = "jdbc:ucanaccess://files/PLZ_Datenbank.accdb";
	private Connection verbindung;
	private Statement befehl;

	PLZDBModel() {
		// Treiber Laden
		try {
			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			verbindung = DriverManager.getConnection(accessConnect);
			befehl = verbindung.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean setPlz(String plz) {
		this.plz = plz;
		this.ort = "";
		this.bLand = "";
		try {
			ResultSet rs = befehl.executeQuery("select Ort from tblPLZ where PLZ = " + plz);
			while (rs.next()) {
				ort += ", " + rs.getString("Ort");
			}
			rs = befehl.executeQuery("select Bundesland from tblPLZ where PLZ = " + plz);
			while (rs.next()) {
				bLand += ", " + rs.getString("Bundesland");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (!ort.equals("") && !bLand.equals("")) {
			ort = ort.substring(2);
			bLand = bLand.substring(2);
			return true;
		}
		return false;
	}

	@Override
	public String getPlz() {
		return plz;
	}

	@Override
	public String getOrt() {
		return ort;
	}

	@Override
	public String getbLand() {
		return bLand;
	}
}
