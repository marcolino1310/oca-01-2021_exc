package ueb_detmar.plzSuche;

public interface PLZModel {
	enum Modus {
		FILE, DB
	}

	public boolean setPlz(String plz);

	public String getPlz();

	String getOrt();

	public String getbLand();

	static PLZModel getInstance(Modus modus) {
		if (modus == Modus.FILE)
			return new PLZFileModel();
		else if (modus == Modus.DB)
			return new PLZDBModel();
		return null;
	}
}
