package ueb_hensel;

import java.util.Arrays;

public class PizzaService {

	public static void main(String[] args) {
		if (args.length == 0) {
			throw new IllegalToppingException("Die Pizza muss einen Belag haben!");
		} else if (args.length > 3) {
			throw new IllegalToppingException("Die Pizza kann maximal drei Bel�ge haben!");
		}
		
//		String[] belaege = {"Tomaten", "K�se", "Salami", "Schinken", "Artischocken", "Pilze", "Broccoli"};
//		Arrays.sort(belaege);
//		for (String belag : args) {
//			if (Arrays.binarySearch(belaege, belag) < 0) {
//				throw new IllegalToppingException("Diesen Belag f�hren wir nicht:" + belag);
//			}
//		}
//		List<String> belaegeList = Arrays.asList(belaege);
//		for (String belag : args) {
//			if (!belaegeList.contains(belag)) {
//				throw new IllegalToppingException("Diesen Belag f�hren wir nicht:" + belag);
//			}
//		}
		for (String belag : args) {
			try {
				Belag.valueOf(belag.toUpperCase());
			} catch (IllegalArgumentException e) {
				throw new IllegalToppingException("Diesen Belag f�hren wir nicht: " + belag);
			}
		}
		
		
		System.out.println("Hier kommt Ihre Pizza!!!");
		System.out.println(Arrays.toString(args));
		System.out.println("Auf Wiedersehen bis zum n�chsten Mal");
	}

}

enum Belag{
	TOMATEN, K�SE, SALAMI, SCHINKEN, ARTISCHOCKEN, PILZE, BROCCOLI;
}

class IllegalToppingException extends RuntimeException {
	IllegalToppingException(String messsage) {
		super(messsage);
	}
}
