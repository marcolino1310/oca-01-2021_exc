package ueb_mueller;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class ZinsrechnerView extends JFrame{
	private static final long serialVersionUID = 1L;
	// Deklaration der Komponenten
	
	// Bezeichnungsfelder -> Label
	JLabel lblStartkapital;
	JLabel lblZinssatz;
	JLabel lblLaufzeit;
	JLabel lblEndkapital;
	JLabel lblTitle;
	
	// Textfelder
	JTextField txfStartkapital;
	JTextField txfZinssatz;
	JTextField txfLaufzeit;
	JTextField txfEndkapital;
	
	// Textbereich
	JTextArea txaVerlauf;
	
	// ScrollPane
	JScrollPane scpVerlauf;
	
	// Befehls-Schaltfl�chen
	JButton btnBerechnen;
	JButton btnBeenden;
	JButton btnLoeschen;
		
	// Optionsfelder - Radio Buttons
	JRadioButton rbtEndkapital;
	JRadioButton rbtLaufzeit;
	
	// ButtonGroup zum gruppieren der Optionsfelder
	ButtonGroup btgModus;
	
	// Container f�r die Komponenten (JPanel)
	JPanel pnlInhalt;
	JPanel pnlTitle;
	JPanel pnlGesamt;
	JPanel pnlButtons;
	JPanel pnlVerlauf;
	
	// Controller als Member-Variable
	ZinsrechnerController controller = new ZinsrechnerController(this);
	
	public ZinsrechnerView() {
		super("Zinsrechner"); // Titelleiste
// 		this.setTitel("Zinsrechner"); // Alternative
		initComponents(); // Initalisierung der Componenten
		
//		Hinzuf�gen Komponenten zum Container
		pnlTitle.add(lblTitle);
		pnlInhalt.add(lblStartkapital);
		pnlInhalt.add(txfStartkapital);
		pnlInhalt.add(lblZinssatz);
		pnlInhalt.add(txfZinssatz);
		pnlInhalt.add(lblLaufzeit);
		pnlInhalt.add(txfLaufzeit);
		pnlInhalt.add(lblEndkapital);
		pnlInhalt.add(txfEndkapital);
		pnlInhalt.add(rbtEndkapital);
		pnlInhalt.add(rbtLaufzeit);
		btgModus.add(rbtEndkapital);
		btgModus.add(rbtLaufzeit);
		pnlButtons.add(btnBerechnen);
		pnlButtons.add(btnLoeschen);
		pnlButtons.add(btnBeenden);
		pnlVerlauf.add(scpVerlauf);
		pnlGesamt.add(lblTitle);
		pnlGesamt.add(pnlInhalt);
		pnlGesamt.add(pnlButtons);
		pnlGesamt.add(pnlVerlauf);
		
//		ActionListener f�r den Button Beenden ohne Model-View-Controller
//		btnBeenden.addActionListener(e -> System.exit(ABORT));
//		btnBeenden.addActionListener(new ActionListener() {
//			
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				System.exit(0);
//				
//			}
//		});
		
		// ActionListener f�r den Button Berechnen/Beenden bei Verwendung mit Model-View-Controller 
		btnBerechnen.addActionListener(controller);
		btnLoeschen.addActionListener(controller);
		btnBeenden.addActionListener(controller);
		rbtEndkapital.addActionListener(controller);
		rbtLaufzeit.addActionListener(controller);
		
		// KeyListener f�r die Textfelder
		txfStartkapital.addKeyListener(controller);
		txfZinssatz.addKeyListener(controller);
		txfLaufzeit.addKeyListener(controller);
		txfEndkapital.addKeyListener(controller);
		
//		Layout des Panels setzen
		pnlInhalt.setLayout(new GridLayout(5, 2 , 30 , 30));
		pnlButtons.setLayout(new FlowLayout(FlowLayout.CENTER));
		pnlGesamt.setLayout(new FlowLayout(FlowLayout.CENTER, 2000, 30));
		rbtEndkapital.setSelected(true); // Optionsfeld "Endkapital" ist beim Start aktiviert
		txfEndkapital.setEditable(false); // Textfeld Endkapital nicht editierbar
		txaVerlauf.setFont(new Font ("Courier New", Font.PLAIN, 12));
		scpVerlauf.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		
//		Setzen des Panels als Inhaltsbereich
		this.setContentPane(pnlGesamt);
		this.setSize(350, 650); // Fenster -> Breite x H�he
		this.setLocationRelativeTo(null); // Fenster -> mittige Anordnung
//		this.setDefaultCloseOperation(EXIT_ON_CLOSE); // Anwendung wird beim Schlie�en des Fensters beendet
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE); // Schlie�en des Fensters wird durch Listener abgedeckt
		this.setVisible(true); // Fesnster wird sichtbar gemacht
	}
	
	public void initComponents() {
		lblTitle = new JLabel("Zinsrechner");
		lblStartkapital = new JLabel("Startkapital");
		lblZinssatz = new JLabel("Zinssatz");
		lblLaufzeit = new JLabel("Laufzeit");
		lblEndkapital= new JLabel("Endkapital");
			
		txfStartkapital = new JTextField(10);
		txfZinssatz = new JTextField(10);
		txfLaufzeit = new JTextField(10);
		txfEndkapital = new JTextField(10);
		
		txaVerlauf = new JTextArea(10,10);
		scpVerlauf = new JScrollPane(txaVerlauf);
		
		rbtEndkapital = new JRadioButton("Endkapital");
		rbtLaufzeit = new JRadioButton("Laufzeit");
		btgModus = new ButtonGroup();
		
		btnBerechnen = new JButton("Berechnen");
		btnBeenden = new JButton("Beenden");
		btnLoeschen = new JButton("L�schen");
		
		pnlInhalt = new JPanel();
		pnlTitle = new JPanel();
		pnlGesamt = new JPanel();
		pnlButtons = new JPanel();
		pnlVerlauf = new JPanel();
	}
	
}
