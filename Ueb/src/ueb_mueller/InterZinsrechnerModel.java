package ueb_mueller;

interface InterZinsrechnerModel {
	public double berechneEndkapital (int startkapital, double zinssatz, int laufzeit);
	
	public double berechneLaufzeit (int startkapital, double zinssatz, int endkapital);
}
