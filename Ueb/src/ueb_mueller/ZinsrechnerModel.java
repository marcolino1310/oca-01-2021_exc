package ueb_mueller;



public class ZinsrechnerModel implements InterZinsrechnerModel {

	@Override
	public double berechneEndkapital(int startkapital, double zinssatz, int laufzeit) {
		return startkapital * Math.pow(1 + zinssatz / 100, laufzeit);
	}

	@Override
	public double berechneLaufzeit(int startkapital, double zinssatz, int endkapital) {
		return Math.log(endkapital/startkapital) / Math.log(1 + zinssatz / 100);
	}	
	
//	public double berechneEndkapital (int startkapital, double zinssatz, int laufzeit) {
//		double endkapital = startkapital * Math.pow((1 + (zinssatz/100)), laufzeit); 
//		
//		return endkapital;
//	}
//	
//	public double berechneLaufzeit (int startkapital, double zinssatz, int endkapital) {
//		double laufzeit = Math.log((endkapital/startkapital))/Math.log1p(zinssatz/100);
//		
//		return laufzeit;
//	}
	
	
	
}
