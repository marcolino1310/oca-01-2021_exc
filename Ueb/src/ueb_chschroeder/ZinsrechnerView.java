package ueb_chschroeder;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class ZinsrechnerView extends JFrame{
	
	private static final long serialVersionUID 	= 1L;
	/**
	 * Zun�chst werden alle Labels, Textfelder, Panels etc. definiert.
	 * 
	 * 
	 *  JLabel
	 *  Labels f�r die Beschriftung der Felder in den Fenstern	
	 */
	 JLabel lblStartkapital;
	 JLabel lblZinssatz;
	 JLabel lblLaufzeit;
	 JLabel lblEndkapital;
	 JLabel lblTitel;
	
	 /**
	  * JTextField
	  * Textfelder, die angezeigt werden und mit den lbls beschriftet werden
	  */
	 JTextField txtStartkapital;
	 JTextField txtZinssatz;
	 JTextField txtLaufzeit;
	 JTextField txtEndkapital;
	
	 /**
	  * JTextArea 
	  * gr�sseres Textfeld, in dem der errechnete Kapitalbetrag
	  * f�r jedes Jahr der Laufzeit aufgelistet wird.
	  */
	 JTextArea txtArea;
	
	 /**
	  *	JButton
	  *	Mit diesen Button wird eine "Aktion" ausgel�st
	  *	f�r das Berechnen, f�r das L�schen der Felder und 
	  *	f�r das Beenden des Programms.	
	  */
	 JButton btnBerechnen;
	 JButton btnClrFields;
	 JButton btnBeenden;
	
	 /**
	  *  JRadio-Button
	  *  mit diesen Button kann ausgew�hlt werden, 
	  *  ob das Kapital oder die Laufzeit berechnet wird.
	  *  die Button sind in einer "ButtonGroup" zusammengefasst f�r die Anzeige
	  */
	 JRadioButton rbtEndkapital;
	 JRadioButton rbtLaufzeit;
	 ButtonGroup btgSelections;
	 ButtonGroup btgModus;
	
	 /**
	  * JPanel
	  * die verschiedenen Panels, die zu einem gesamtPanel zusammengesetzt werden
	  * 
	  * pnlInhalt = 	5 Ziele, 2 Spalten (Beschriftung, Feld) 
	  * 				4 Zeilen f�r Startkapital, Zinssatz, Laufzeit, Gesamtkapital
	  * 				5. Zeile f�r die Anzeige der RadioButton
	  * 
	  * pnlSelections = das Panel f�r die Auswahl "Berechnen, l�schen, Beenden"
	  * 
	  * pnlTxtAerea	= das Panel f�r das "gro�e" Ausgabetextfeld der Jahressummen
	  * 
	  * 
	  * JPanel nennt man entsprechend auch "Container", da sie etwas beinhalten wie ein Container
	  *
	  * pnlTitel f�r das lblTitel	= "Zinsrechner"	�berschrift/Titel
	  * 
	  * pnlGesamt	= der "Container" der alles vereint und f�r die Postion auf dem Bildschirm verantwortlich ist
	  * 				das Setzten der Postionen und Gr��en erfolgt sp�ter im "initComponents"
	  */
	 JPanel pnlInhalt;		// "TeilPanel" - ist ein Teil des Gesamt Panels
	 JPanel pnlTitel;		// "TeilPanel" - ist ein Teil des Gesamt Panels
	 JPanel pnlSelections;
	 JPanel pnlTxtArea;
	 JPanel pnlGesamt;		// "GesamtPanel" - ist das Gesamt Panels
	
	 /** 
	  * um die txtArea wird ein "Scroll-Feld" gelegt, sobald das Feld "�berf�llt" ist, kann man scrollen.
	  * vertikal wir horizontal	
	  */
	 JScrollPane scroll;
	 
	 /**
	  * 
	  * Definieren eines controller	(ActionLlistener)
	  * 
	  */
	 // Controller als Member-Variable	
	 ZinsrechnerController controller = new ZinsrechnerController(this);
	
	
	
	public  ZinsrechnerView() {
		
		/**
		 * Aufbau der View (Ansicht der Panels auf dem Bildschirm)
		 */
		super("Zinsrechner");	// Titelleiste

		/**
		 * Initialisieren der Komponenten		
		 */	
		initComponents();
		
		/**
		 * Die Panels mit den dazugeh�rigen Textfeldern und Button best�cken		
		 */
		// Titel-Panel
		pnlTitel.add(lblTitel);
		// Inhalt-Panel
		pnlInhalt.add(lblStartkapital);
		pnlInhalt.add(txtStartkapital);
		pnlInhalt.add(lblZinssatz);
		pnlInhalt.add(txtZinssatz);
		pnlInhalt.add(lblLaufzeit);
		pnlInhalt.add(txtLaufzeit);
		pnlInhalt.add(lblEndkapital);
		pnlInhalt.add(txtEndkapital);
		pnlInhalt.add(rbtEndkapital);
		pnlInhalt.add(rbtLaufzeit);
		btgModus.add(rbtEndkapital);
		btgModus.add(rbtLaufzeit);
		// Auswahl-Panel
		pnlSelections.add(btnBerechnen);
		pnlSelections.add(btnClrFields);
		pnlSelections.add(btnBeenden);
		btgSelections.add(btnBerechnen);
		btgSelections.add(btnClrFields);
		btgSelections.add(btnBeenden);
		// Textfeld-Panel zum scrollen
		pnlTxtArea.add(scroll);
		// Gesamt-Panel
		pnlGesamt.add(pnlTitel);
		pnlGesamt.add(pnlInhalt);
		pnlGesamt.add(pnlSelections);
		pnlGesamt.add(pnlTxtArea);
		
		// WindowListener (Alt+F4 oder X)
		this.addWindowListener(controller);
		
		// die ActionListener f�r den Controller definieren
		btnBerechnen.addActionListener(controller);
		btnBeenden.addActionListener(controller);
		btnClrFields.addActionListener(controller);
		rbtEndkapital.addActionListener(controller);
		rbtLaufzeit.addActionListener(controller);

		// Layout der Panels setzen
		pnlInhalt.setLayout(new GridLayout(5,2,10,10));
		pnlSelections.setLayout(new FlowLayout(FlowLayout.CENTER));
		pnlGesamt.setLayout(new FlowLayout(FlowLayout.CENTER, 2000, 30));
		rbtEndkapital.setSelected(true);
		txtEndkapital.setEditable(false);
		txtArea.setEditable(false);
		txtArea.setFont(new Font("Curier New", Font.PLAIN,12));
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		// Gr��e und Position des Gesamt-Panels setzten
		this.setContentPane(pnlGesamt);
		this.setSize(300,600); // Gr��e
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.setVisible(true);
		
	}
	
	/**
	 * Componenten initialisieren
	 */
	private void initComponents() {

		/**
		 * Label f�r die Beschriftung der Felder "beschriften"
		 */
		lblStartkapital	= new JLabel("Startkapital :");
		lblZinssatz 	= new JLabel("Zinssatz     :");
		lblLaufzeit 	= new JLabel("Laufzeit     :");
		lblEndkapital 	= new JLabel("Endkapital   :");
		lblTitel		= new JLabel("Zinsrechner");
		
		/**
		 * Textfeldgr��en definieren
		 */
		txtStartkapital	= new JTextField(10);
		txtZinssatz 	= new JTextField(10);
		txtLaufzeit 	= new JTextField(10);
		txtEndkapital 	= new JTextField(10);
		
		/**
		 * das "gro�e" Testfeld definieren
		 */
		txtArea			= new JTextArea(10,20);
		
		
		/**
		 *	RadiButton beschriften und gruppieren 
		 */
		rbtEndkapital 	= new JRadioButton("Endkapital");
		rbtLaufzeit		= new JRadioButton("Laufzeit");
		btgModus 		= new ButtonGroup();
		
		/**
		 * die Button f�r die Auswahl definieren und beschriften
		 */
		btnBerechnen	= new JButton("Berechnen");
		btnClrFields	= new JButton("L�schen");
		btnBeenden 		= new JButton("Beenden");
		btgSelections 	= new ButtonGroup();
		
		/**
		 * das "Scroll-Panel" initialisieren
		 */
		scroll 			= new JScrollPane(txtArea);
		
		/**
		 * Die Panels selbst erstellen
		 */
		pnlInhalt 		= new JPanel();
		pnlSelections	= new JPanel();
		pnlTitel		= new JPanel();
		pnlTxtArea		= new JPanel();
		pnlGesamt		= new JPanel();

		/**
		 * Ein bischen Farbe ins Spiel bringen
		 */
		pnlGesamt.setForeground(Color.yellow);
		pnlGesamt.setBackground(Color.yellow);
		pnlSelections.setForeground(Color.yellow);
		pnlSelections.setBackground(Color.yellow);
		pnlInhalt.setForeground(Color.yellow);
		pnlInhalt.setBackground(Color.yellow);
	}
}
