package ueb_chschroeder;
// 
public interface InterfaceZinsrechnerModel {
	double berechneEndkapital(int startkapital, double zinssatz, int laufzeit);
	double berechneLaufzeit(int startkapital, double zinssatz, int endkapital);
}
