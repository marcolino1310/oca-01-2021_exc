package ueb_chschroeder;

/**
 * Berechnungsroutinen f�r
 * 
 * Endkapital anhand der Vorgaben
 * 			int		Startkapital
 * 			double	Zinssatz
 * 			int		Laufzeit
 * 
 *  Laufzeit anhand der Vorgaben
 *  		int		Startkapital
 *  		double	Zinssatz
 *  		int 	Endkapital	
 */

public class ZinsrechnerModel implements InterfaceZinsrechnerModel{
	
	@Override
	public double berechneEndkapital(int startkapital, double zinssatz, int laufzeit) {
		return startkapital *Math.pow(1+zinssatz/100,  laufzeit);
	}
	
	@Override
	public double berechneLaufzeit(int startkapital, double zinssatz, int endkapital) {
		return Math.log((double)endkapital/startkapital)/Math.log1p(zinssatz/100);
	}

}
