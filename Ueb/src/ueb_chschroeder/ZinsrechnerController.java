package ueb_chschroeder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.text.NumberFormat;
import java.util.Formatter;

import javax.swing.JOptionPane;

import java.awt.event.WindowEvent;

public class ZinsrechnerController extends WindowAdapter implements ActionListener{
	
	ZinsrechnerView view;
	ZinsrechnerModel model;
	
	public ZinsrechnerController(ZinsrechnerView view) {
		this.view = view;
		this.model = new ZinsrechnerModel();
	}
	
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == view.btnBeenden) {
			beenden();
		}
			else if(e.getSource()== view.btnBerechnen) {
				berechnen();
			
		}
			else if(e.getSource()==view.rbtEndkapital){
				clrFields();
				view.txtEndkapital.setEditable(false);
				view.txtLaufzeit.setEditable(true);
			}
			else if(e.getSource()==view.rbtLaufzeit){
				clrFields();
				view.txtEndkapital.setEditable(true);
				view.txtLaufzeit.setEditable(false);
				
			}
			else if(e.getSource()==view.btnClrFields) {
				clrFields();
				view.rbtEndkapital.setSelected(true);
				view.rbtLaufzeit.setSelected(false);
				view.txtEndkapital.setEditable(false);
				view.txtLaufzeit.setEditable(true);
				
			}
	}
	
	private void berechnen() {
		// Inhalt der drei Testfelder auslesen (getText)
		// Umwandeln in die richtigen Datentypen
		// endkapital = berechneEndkapital(startkapital,zinssatz,laufzeit);
		// Aufruf der berechneEndkapital-Methode aus dem Model
		// Ergebnis formatiert in Textfeld txtEndkapital aus der view eintragen (setText)
		
		int startkapital = Integer.parseInt(view.txtStartkapital.getText());
		double zinssatz = Double.parseDouble(view.txtZinssatz.getText());
		double errechnetesEndkapital =  0.00;
		double errechneteLaufzeit = 0.00;
		
		if(view.rbtEndkapital.isSelected()) {
			int gesetztelaufzeit = Integer.parseInt(view.txtLaufzeit.getText());
			view.txtEndkapital.setText(NumberFormat.getCurrencyInstance().									// Ausgabeformat f�r
								   format(errechnetesEndkapital = 										// die Endkapitalsumme, die
								   				model.berechneEndkapital(startkapital,
								   										 zinssatz,
								   										 gesetztelaufzeit)));			// hier berechnet wird.
			zinsVerlauf(gesetztelaufzeit, zinssatz, startkapital);
		}
		else if(view.rbtLaufzeit.isSelected()) {
			int gesetztesEndkapital = Integer.parseInt(view.txtEndkapital.getText());
			view.txtLaufzeit.setText(String.format("%.1f",(errechneteLaufzeit = 
												   				model.berechneLaufzeit(startkapital, 
												   									   zinssatz, 
												   									   gesetztesEndkapital))));
			zinsVerlauf((int)errechneteLaufzeit, zinssatz, startkapital);
		}
	}
	
	private void clrFields() {
		// Inhalte der 4 Textfelder l�schen
		// getComponents
		view.txtEndkapital.setText("");
		view.txtLaufzeit.setText("");
		view.txtStartkapital.setText("");
		view.txtZinssatz.setText("");
		view.txtArea.setText("");
		view.txtStartkapital.requestFocus();
		
	}
	
	
	// txtArea Textfeld unterhalb mit Werten f�llen
	// das Berechnen wir leider doppelt und dreifach durchgef�hrt, 
	private void zinsVerlauf(int laufzeit, double zinssatz, int startkapital) {
		// view.txtArea.setText("");
		StringBuilder sbVerlauf = new StringBuilder();
		for(int i=0; i<=laufzeit; i++) {
			sbVerlauf.append(i+" Jahre "+
								String.format("%2d\t%,15.2f �%n", i,model.berechneEndkapital(startkapital, zinssatz, i)));
		}
		view.txtArea.setText(sbVerlauf.toString());
	}

	private void beenden() {
		int antwort =JOptionPane.showConfirmDialog(view, "Sind Sie sicher?", "Beenden", JOptionPane.YES_NO_OPTION);
		if(antwort == JOptionPane.YES_OPTION) {
			System.exit(0);
		}
	}
	@Override
	public void windowClosing(WindowEvent e) {
		beenden();
	}

}
